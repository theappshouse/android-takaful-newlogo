package com.appshouse.takaful.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.QuoteBaitakActivity;
import com.appshouse.takaful.activities.QuoteFireActivity;
import com.appshouse.takaful.activities.QuoteMotorActivity;
import com.appshouse.takaful.activities.QuoteTravelActivity;
import com.appshouse.takaful.activities.QuoteUserInfoActivity;
import com.appshouse.takaful.attributes.Product;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.constant.ProductCodes;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mohammed Algassab on 1/31/2016.
 */
public class QuoteProductAdapter extends RecyclerView.Adapter<QuoteProductAdapter.ProductViewHolder> {

    private List<Product> productList;
    private Context context;


    public static class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView tvProductTitle;
        ImageView ivProduct;
        LinearLayout lloRow;

        public ProductViewHolder(View itemView, Context context) {
            super(itemView);
            tvProductTitle = (TextView) itemView.findViewById(R.id.tvProductTitle);
            tvProductTitle.setTypeface(MyMethods.getBoldFont(context));
            ivProduct = (ImageView) itemView.findViewById(R.id.ivProduct);
            lloRow = (LinearLayout) itemView.findViewById(R.id.lloRow);
        }
    }

    public QuoteProductAdapter(Context context, List<Product> productList) {
        this.productList = productList;
        this.context = context;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_quote_product, parent, false);
        return new ProductViewHolder(rowView, context);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        final Product product = productList.get(position);
        holder.tvProductTitle.setText(product.mTitle);
        Picasso.with(context).load(APIs.ImageURL + product.mThumbnail).into(holder.ivProduct);
        holder.lloRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!new SharedPreferencesClass(context).isLogin()) {
                    Intent intent = new Intent(context, QuoteUserInfoActivity.class);
                    intent.putExtra("productCode", product.mCode);
                    context.startActivity(intent);
                } else {
                    switch (product.mCode) {
                        case ProductCodes.MOTOR:
                            context.startActivity(new Intent(context, QuoteMotorActivity.class));
                            break;

                        case ProductCodes.TRAVEL:
                            context.startActivity(new Intent(context, QuoteTravelActivity.class));
                            break;

                        case ProductCodes.FIRE:
                            context.startActivity(new Intent(context, QuoteFireActivity.class));
                            break;

                        case ProductCodes.BAITAK:
                            context.startActivity(new Intent(context, QuoteBaitakActivity.class));
                            break;

                        default:
                            Toast.makeText(context, context.getString(R.string.not_available), Toast.LENGTH_SHORT).show();
                            break;
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

}

