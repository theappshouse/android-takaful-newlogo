package com.appshouse.takaful.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.ClaimDetailActivity;
import com.appshouse.takaful.attributes.Claim;
import com.appshouse.takaful.utilities.MyMethods;

import java.util.List;

/**
 * Created by Mohammed Algassab on 1/6/2016.
 */
public class ClaimAdapter extends RecyclerView.Adapter<ClaimAdapter.ClaimViewHolder> {

    private List<Claim> claimList;
    private Context context;
    Typeface font;

    public static class ClaimViewHolder extends RecyclerView.ViewHolder {
        TextView tvClaimNo;
        TextView tvStatus;
        LinearLayout lloStatus;
        LinearLayout lloRow;

        public ClaimViewHolder(View itemView, Typeface font) {
            super(itemView);
            tvClaimNo = (TextView) itemView.findViewById(R.id.tvClaimNo);
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
            lloStatus = (LinearLayout) itemView.findViewById(R.id.lloStatus);
            lloRow = (LinearLayout) itemView.findViewById(R.id.lloRow);

            tvClaimNo.setTypeface(font);
            tvStatus.setTypeface(font);
        }
    }

    public ClaimAdapter(Context context, List<Claim> claimList) {
        this.claimList = claimList;
        this.context = context;
        font = MyMethods.getRegularFont(context);
    }

    @Override
    public ClaimViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate
                (R.layout.row_claims, parent, false);
        return new ClaimViewHolder(rowView,font);
    }

    @Override
    public void onBindViewHolder(ClaimViewHolder holder, int position) {
        final Claim claim = claimList.get(position);
        holder.tvClaimNo.setText(claim.mNo);
        switch (claim.mStatus) {
            case 1:
                holder.tvStatus.setText(R.string.status_claim_pending);
                holder.lloStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.policy_pending));
                break;
            case 2:
                holder.tvStatus.setText(R.string.status_claim_accepted);
                holder.lloStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.status_issued));
                break;
            case 3:
                holder.tvStatus.setText(R.string.status_claim_pending);
                holder.lloStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.policy_expired));
                break;
        }

        holder.lloRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("clikbtn","iclick") ;
                Intent intent = new Intent(context, ClaimDetailActivity.class);
                intent.putExtra("claimNo", claim.mNo);
                intent.putExtra("status", claim.mStatus);
                intent.putExtra("reasonRejection", claim.mReasonRejection);
                intent.putExtra("claimDetails", claim.mClaimDetailObject.toString());
                context.startActivity(intent);
            }
        });
        /*holder.tvStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("clikbtn","iclick") ;
                Intent intent = new Intent(context, ClaimDetailActivity.class);
                intent.putExtra("claimNo", claim.mNo);
                intent.putExtra("status", claim.mStatus);
                intent.putExtra("reasonRejection", claim.mReasonRejection);
                intent.putExtra("claimDetails", claim.mClaimDetailObject.toString());
                context.startActivity(intent);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return claimList.size();
    }

}




