package com.appshouse.takaful.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.attributes.Agency;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.MyMethods;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mohammed Algassab on 1/31/2016.
 */
public class AgencyAdapter extends RecyclerView.Adapter<AgencyAdapter.AgencyViewHolder> {

    private List<Agency> agencyList;
    private Context context;

    public static class AgencyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvName;
        TextView tvTimeService, tvTimeSales;
        TextView tvPhone, tvAddress;
        ImageView ivAgency;
        LinearLayout lloPhone, lloNavigate;

        public AgencyViewHolder(View itemView, Context context) {
            super(itemView);
            ivAgency = (ImageView) itemView.findViewById(R.id.ivIcon);
            tvTitle = (TextView) itemView.findViewById(R.id.tvAgency);
            tvName = (TextView) itemView.findViewById(R.id.tvAgencyInfo);
            tvTimeService = (TextView) itemView.findViewById(R.id.tvTimeService);
            tvTimeSales = (TextView) itemView.findViewById(R.id.tvTimeSales);
            tvPhone = (TextView) itemView.findViewById(R.id.tvPhone);
            tvAddress = (TextView) itemView.findViewById(R.id.tvAddress);
            tvTitle.setTypeface(MyMethods.getBoldFont(context));
            tvName.setTypeface(MyMethods.getRegularFont(context));
            lloPhone = (LinearLayout) itemView.findViewById(R.id.lloPhone);
            lloNavigate = (LinearLayout) itemView.findViewById(R.id.lloNavigate);
        }
    }

    public AgencyAdapter(Context context, List<Agency> agencyList) {
        this.agencyList = agencyList;
        this.context = context;
    }

    @Override
    public AgencyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_agency, parent, false);
        return new AgencyViewHolder(rowView, context);
    }

    @Override
    public void onBindViewHolder(AgencyViewHolder holder, int position) {
        final Agency agency = agencyList.get(position);
        holder.tvTitle.setText(agency.mTitle.toUpperCase());
        holder.tvName.setText(agency.mName);
        holder.tvTimeService.setText(Html.fromHtml(agency.mDayService.toUpperCase()) + "\n" + Html.fromHtml(agency.mTimeService.toUpperCase()));
        holder.tvTimeSales.setText(Html.fromHtml(agency.mDaySales.toUpperCase()) + "\n" + Html.fromHtml(agency.mTimeSales.toUpperCase()));
        holder.tvPhone.setText(agency.mPhone);
        holder.tvAddress.setText(agency.mAddress);
        //holder.ivAgency.setImageResource(agency.mLogo);

        if (agency.mLatitude == 0 || agency.mLongitude == 0) {
            holder.lloNavigate.setEnabled(false);
            holder.lloNavigate.setBackgroundColor(ContextCompat.getColor(context,R.color.disable_transparent_button));
        } else {
            holder.lloNavigate.setEnabled(true);
            holder.lloNavigate.setBackground(context.getResources().getDrawable(R.drawable.button_transparent));
            holder.lloNavigate.setOnClickListener(new View.OnClickListener() {
                @Override

                public void onClick(View v) {
                    MyMethods.openGoogleMap(context, String.valueOf(agency.mLatitude),
                            String.valueOf(agency.mLongitude), agency.mTitle);
                }
            });
        }

        holder.lloPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMethods.dailCall(context, agency.mPhone);
            }
        });
        Picasso.with(context).load(APIs.ImageURL + agency.mLogoPath).into(holder.ivAgency);
    }

    @Override
    public int getItemCount() {
        return agencyList.size();
    }

}

