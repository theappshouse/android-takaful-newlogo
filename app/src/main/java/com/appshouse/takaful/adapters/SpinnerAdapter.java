package com.appshouse.takaful.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.utilities.MyMethods;


/**
 * Created by Mohammed Algassab on 2/21/2015.
 */
public class SpinnerAdapter extends ArrayAdapter<String> {
    LayoutInflater inflater;
    Context context;
    Typeface font;

    public SpinnerAdapter(Context context, String[] objects) {
        super(context, 0, objects);
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        font = MyMethods.getRegularFont(context);
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        TextView tvSpinnerItem = (TextView) convertView.findViewById(android.R.id.text1);
        tvSpinnerItem.setTypeface(font);
        String spinnerItem = getItem(position);
        tvSpinnerItem.setText(spinnerItem);
        return convertView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.spinner_view, parent, false);
        TextView tvSpinnerItem = (TextView) convertView.findViewById(R.id.tvTitle);
        tvSpinnerItem.setTypeface(font);
        String spinnerItem = getItem(position);
        tvSpinnerItem.setText(spinnerItem);
        return convertView;
    }
}
