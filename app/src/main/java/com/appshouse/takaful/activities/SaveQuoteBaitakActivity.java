package com.appshouse.takaful.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.SpinnerAdapter;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/4/2016.
 */
public class SaveQuoteBaitakActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Toolbar toolbar;
    double premium,sumAssured;
    TextView tvPremium,tvSumAssured;
    EditText etHouse, etRoad, etBlock;
    Spinner sCity;
    SharedPreferencesClass setting;
    String selectedCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_baitak_quote);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_saveBaitakQuote);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tvPremium = (TextView) findViewById(R.id.tvPremium);
        tvSumAssured = (TextView) findViewById(R.id.tvSumAssured);
        etHouse = (EditText) findViewById(R.id.etHouse);
        etRoad = (EditText) findViewById(R.id.etRoad);
        etBlock = (EditText) findViewById(R.id.etBlock);
        sCity = (Spinner) findViewById(R.id.sCity);

        sCity.setOnItemSelectedListener(this);
        setting = new SharedPreferencesClass(this);

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        premium = getIntent().getDoubleExtra("premium", 0);
        sumAssured = getIntent().getDoubleExtra("sumAssured", 0);
        tvPremium.setText(String.valueOf(premium));
        tvSumAssured.setText(String.valueOf(sumAssured));

        getCities();
    }

    public void getCities() {
        String Tag_Request = "getCities";
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.setCancelable(false);
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetCities
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                setSpinnerData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                MyMethods.showNetworkErrorDialog(SaveQuoteBaitakActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCities();
                    }
                });
            }
        });
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    String[] cityArray;
    private void setSpinnerData(JSONObject jsonObject) {
        try {
            if (jsonObject.getBoolean("success")) {
                JSONArray resultArray = jsonObject.getJSONArray("result");
                cityArray = new String[resultArray.length()];
                for (int i = 0; i < resultArray.length(); i++) {
                    cityArray[i] = resultArray.getJSONObject(i).getString("Name");
                }

                sCity.setAdapter(new SpinnerAdapter(this, cityArray));
            } else {
                MyMethods.showNetworkErrorDialog(SaveQuoteBaitakActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getCities();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String cpr, house, road, block, city;
    private void prepareSaveQuote() {
        if (setting.isLogin())
            cpr = setting.getCpr();
        else
            cpr = getIntent().getStringExtra("cpr");

        house = etHouse.getText().toString();
        road = etRoad.getText().toString();
        block = etBlock.getText().toString();
        city = selectedCity;
        if (cpr.isEmpty() || house.isEmpty() || road.isEmpty() || block.isEmpty() || city.isEmpty()) {
            Snackbar.make(findViewById(R.id.main), R.string.message_enterRequiredData, Snackbar.LENGTH_INDEFINITE).show();
        } else {
            saveQuote();
        }
    }

    String Tag_Request = "saveQuote";

    private void saveQuote() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetSaveBaitakQuote
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getSaveQuoteResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                saveQuote();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Intent i = getIntent();
                params.put("premium", String.valueOf(premium));
                params.put("policyStartDate", i.getStringExtra("policyStartDate"));
                params.put("S1Building", i.getStringExtra("S1Building"));
                params.put("house", house);
                params.put("road", road);
                params.put("block", block);
                params.put("city", city);
                params.put("household", i.getStringExtra("household"));
                params.put("jewelery", i.getStringExtra("jewelery"));
                params.put("fixtures", i.getStringExtra("fixtures"));
                params.put("furniture", i.getStringExtra("furniture"));
                params.put("cpr", cpr);
                params.put("saveQuote", "1");
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(SaveQuoteBaitakActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void getSaveQuoteResult(final JSONObject jsonObject) {
        try {
            Log.i("executeResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                String quotationNo = jsonObject.getString("result");
                MyMethods.showGeneralDialogMessage(this,
                        getString(R.string.message_quoteSavedSuccess) + " (" + quotationNo + ") ",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(SaveQuoteBaitakActivity.this, MainActivity.class);
                                intent.putExtra("isMyTakafulPage", true);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        });
            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else if (jsonObject.getInt("status") == 7) {
                MyMethods.showNetworkErrorDialog(this, getString(R.string.message_addressSaveFailed), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            retrySaveAddress(jsonObject.getString("result"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                MyMethods.showSnackBarMessage(findViewById(R.id.main), jsonObject.getString(getString(R.string.api_message)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    String Tag_Request_Approve = "SaveAddress";

    private void retrySaveAddress(final String quotationNo) {
        //this function will be called by user if the saving address failed in GetSaveBaitakQuote api
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.SaveAddress
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getSaveQuoteResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //We did not use SnackBar because the user must retry to complete the process perfectly
                mDialog.dismiss();
                MyMethods.showNetworkErrorDialog(SaveQuoteBaitakActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        retrySaveAddress(quotationNo);
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Intent i = getIntent();
                params.put("quotationNo", quotationNo);
                params.put("house", house);
                params.put("road", road);
                params.put("block", block);
                params.put("city", city);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request_Approve);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                MyMethods.hideKeyboard(this);
                prepareSaveQuote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sCity:
                selectedCity = cityArray[position];
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
