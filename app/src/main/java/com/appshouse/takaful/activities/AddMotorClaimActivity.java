package com.appshouse.takaful.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.SpinnerAdapter;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.dialogs.AddProgressDialog;
import com.appshouse.takaful.utilities.AndroidMultiPartEntity;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.nononsenseapps.filepicker.AbstractFilePickerActivity;
import com.nononsenseapps.filepicker.FilePickerActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/16/2016.
 */
public class AddMotorClaimActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    Toolbar toolbar;

    LinearLayout lloPolicyReport, lloVehicleCertificate, lloVehicleCertificateBack, lloLicense,
            lloLicenseBack, lloCpr, lloCprBack, lloPolicyReportedDate, lloTimeOfLoss, lloDateOfLoss,
            lloAccident1, lloAccident2, lloAccident3, lloAccident4, lloAccident5, lloAccident6;

    EditText etDateOfLoss, etTime, etYearNoLicense, etPolicyReportedDate, etReportNo;

    TextView tvCpr, tvCprBack, tvLicense, tvLicenseBack, tvVehicleCertificate, tvVehicleCertificateBack,
            tvPolicyReport, tvAccident1, tvAccident2, tvAccident3, tvAccident4, tvAccident5, tvAccident6;

    Spinner sNatureOfLoss, sPolicyNo;
    
    View view;
    String[] motorPolicyNo;
    String selectedPolicyNo, selectedNatureOfLoss;
    SharedPreferencesClass setting;
    View vAccident2, vAccident3, vAccident4, vAccident5, vAccident6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_motor_claim);
        view = findViewById(android.R.id.content);

        //Create and set the action bar
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.add_motor_claim);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        setting = new SharedPreferencesClass(this);

        lloPolicyReport = (LinearLayout) findViewById(R.id.lloPolicyReport);
        lloVehicleCertificate = (LinearLayout) findViewById(R.id.lloVehicleCertificate);
        lloVehicleCertificateBack = (LinearLayout) findViewById(R.id.lloVehicleCertificateBack);
        lloLicense = (LinearLayout) findViewById(R.id.lloLicense);
        lloLicenseBack = (LinearLayout) findViewById(R.id.lloLicenseBack);
        lloCpr = (LinearLayout) findViewById(R.id.lloCpr);
        lloCprBack = (LinearLayout) findViewById(R.id.lloCprBack);
        lloPolicyReportedDate = (LinearLayout) findViewById(R.id.lloPolicyReportedDate);
        lloTimeOfLoss = (LinearLayout) findViewById(R.id.lloTimeOfLoss);
        lloDateOfLoss = (LinearLayout) findViewById(R.id.lloDateOfLoss);
        lloAccident1 = (LinearLayout) findViewById(R.id.lloAccident1);
        lloAccident2 = (LinearLayout) findViewById(R.id.lloAccident2);
        lloAccident3 = (LinearLayout) findViewById(R.id.lloAccident3);
        lloAccident4 = (LinearLayout) findViewById(R.id.lloAccident4);
        lloAccident5 = (LinearLayout) findViewById(R.id.lloAccident5);
        lloAccident6 = (LinearLayout) findViewById(R.id.lloAccident6);

        vAccident2 = findViewById(R.id.vAccident2);
        vAccident3 = findViewById(R.id.vAccident3);
        vAccident4 = findViewById(R.id.vAccident4);
        vAccident5 = findViewById(R.id.vAccident5);
        vAccident6 = findViewById(R.id.vAccident6);

        etDateOfLoss = (EditText) findViewById(R.id.etDateOfLoss);
        etTime = (EditText) findViewById(R.id.etTime);
        etYearNoLicense = (EditText) findViewById(R.id.etYearNoLicense);
        etPolicyReportedDate = (EditText) findViewById(R.id.etPolicyReportedDate);
        etReportNo = (EditText) findViewById(R.id.etReportNo);

        tvCpr = (TextView) findViewById(R.id.tvCpr);
        tvCprBack = (TextView) findViewById(R.id.tvCprBack);
        tvLicense = (TextView) findViewById(R.id.tvLicense);
        tvLicenseBack = (TextView) findViewById(R.id.tvLicenseBack);
        tvVehicleCertificate = (TextView) findViewById(R.id.tvVehicleCertificate);
        tvVehicleCertificateBack = (TextView) findViewById(R.id.tvVehicleCertificateBack);
        tvPolicyReport = (TextView) findViewById(R.id.tvPolicyReport);
        tvAccident1 = (TextView) findViewById(R.id.tvAccident1);
        tvAccident2 = (TextView) findViewById(R.id.tvAccident2);
        tvAccident3 = (TextView) findViewById(R.id.tvAccident3);
        tvAccident4 = (TextView) findViewById(R.id.tvAccident4);
        tvAccident5 = (TextView) findViewById(R.id.tvAccident5);
        tvAccident6 = (TextView) findViewById(R.id.tvAccident6);
        sNatureOfLoss = (Spinner) findViewById(R.id.sNatureOfLoss);
        sPolicyNo = (Spinner) findViewById(R.id.sPolicyNo);

        sNatureOfLoss.setOnItemSelectedListener(this);
        sPolicyNo.setOnItemSelectedListener(this);
        lloPolicyReport.setOnClickListener(this);
        lloVehicleCertificate.setOnClickListener(this);
        lloVehicleCertificateBack.setOnClickListener(this);
        lloLicense.setOnClickListener(this);
        lloLicenseBack.setOnClickListener(this);
        lloCpr.setOnClickListener(this);
        lloCprBack.setOnClickListener(this);
        lloPolicyReportedDate.setOnClickListener(this);
        lloTimeOfLoss.setOnClickListener(this);
        lloDateOfLoss.setOnClickListener(this);
        lloAccident1.setOnClickListener(this);
        lloAccident2.setOnClickListener(this);
        lloAccident3.setOnClickListener(this);
        lloAccident4.setOnClickListener(this);
        lloAccident5.setOnClickListener(this);
        lloAccident6.setOnClickListener(this);

        sNatureOfLoss.setAdapter(new SpinnerAdapter(this, getResources().getStringArray(R.array.nature_of_claim)));
        selectedNatureOfLoss = getResources().getStringArray(R.array.nature_of_claim)[0];

        getMotorPolicy();
        //setAutoData();
    }

    private void setAutoData() {
        etDateOfLoss.setText("17/2/2016");
        etTime.setText("8:00");
        etReportNo.setText("156155515999");
        etPolicyReportedDate.setText("17/2/2016");
    }

    private void openPickDateDialog(final EditText editText) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mYear, mMonth, mDay;
                mYear = String.valueOf(year);
                mMonth = String.valueOf(monthOfYear + 1);
                mDay = String.valueOf(dayOfMonth);
                String date = mDay + "/" + mMonth + "/" + mYear;
                editText.setText(date);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void openTimePickerDialog() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
                etTime.setText(addZeroIfNeeded(selectedHour) + ":" + addZeroIfNeeded(selectedMinute));
            }
        }, 8, 0, true);
        timePickerDialog.show();
    }

    private String addZeroIfNeeded(int number) {
        String sNumber = String.valueOf(number);
        if (sNumber.length() == 1)
            sNumber = "0" + sNumber;
        return sNumber;
    }

    /**
     * fileTypeId for getting file path .. we get the id so we know for what we are getting the
     * file path in the setFilePath method
     *
     * @see #setFilePath(String)
     */
    int fileTypeId;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lloDateOfLoss:
                openPickDateDialog(etDateOfLoss);
                break;

            case R.id.lloPolicyReportedDate:
                openPickDateDialog(etPolicyReportedDate);
                break;

            case R.id.lloTimeOfLoss:
                openTimePickerDialog();
                break;

            case R.id.lloCpr:
            case R.id.lloCprBack:
            case R.id.lloLicense:
            case R.id.lloLicenseBack:
            case R.id.lloVehicleCertificate:
            case R.id.lloVehicleCertificateBack:
            case R.id.lloPolicyReport:
            case R.id.lloAccident1:
            case R.id.lloAccident2:
            case R.id.lloAccident3:
            case R.id.lloAccident4:
            case R.id.lloAccident5:
            case R.id.lloAccident6:
                fileTypeId = v.getId();
                uploadFileDialog();
                break;
        }
    }

    int FILE_CHOOSER = 0, GalleryAttach_Request = 1, CameraAttach_Request = 2;

    private void uploadFileDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.dialog_title_uploadFile));
        dialog.setMessage(getString(R.string.dialog_message_uploadFile));

        dialog.setPositiveButton(getString(R.string.dialog_button_gallery), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, GalleryAttach_Request);
            }
        });

        dialog.setNeutralButton(getString(R.string.dialog_button_camera), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
                startActivityForResult(cameraIntent, CameraAttach_Request);
            }
        });

//        dialog.setNegativeButton(getString(R.string.dialog_button_file), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                openFileChooser();
//            }
//        });
        dialog.show();
    }

    private void openFileChooser() {
        Intent i = new Intent(this, FilePickerActivity.class);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        } else {
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        }
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
        startActivityForResult(i, AbstractFilePickerActivity.MODE_FILE);
    }

    Uri imageUri;

    private Uri setImageUri() {
        File file = new File(Environment.getExternalStorageDirectory()
                + "/DCIM/", "image" + new Date().getTime() + ".png");
        imageUri = Uri.fromFile(file);
        return imageUri;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FILE_CHOOSER) {
                Uri uri = data.getData();
                setFilePath(uri.getPath());
            } else if (requestCode == GalleryAttach_Request || requestCode == CameraAttach_Request) {
                if (requestCode == GalleryAttach_Request) {
                    imageUri = getImageUriFromCamera(data.getData());
                } else if (requestCode != CameraAttach_Request) {
                    imageUri = null;
                }

                if (imageUri != null) {
                    setFilePath(imageUri.getPath());
                }
            }
        }
    }

    private Uri getImageUriFromCamera(Uri uri) {
        String[] fileColumn = {MediaStore.Images.Media.DATA};
        Cursor imageCursor = getContentResolver().query(uri, fileColumn, null, null, null);
        imageCursor.moveToFirst();
        int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
        return Uri.parse(imageCursor.getString(fileColumnIndex));
    }

    private void setFilePath(String path) {
        switch (fileTypeId) {
            case R.id.lloCpr:
                tvCpr.setText(path);
                break;
            case R.id.lloCprBack:
                tvCprBack.setText(path);
                break;
            case R.id.lloLicense:
                tvLicense.setText(path);
                break;
            case R.id.lloLicenseBack:
                tvLicenseBack.setText(path);
                break;
            case R.id.lloVehicleCertificate:
                tvVehicleCertificate.setText(path);
                break;
            case R.id.lloVehicleCertificateBack:
                tvVehicleCertificateBack.setText(path);
                break;
            case R.id.lloPolicyReport:
                tvPolicyReport.setText(path);
                break;
            case R.id.lloAccident1:
                tvAccident1.setText(path);
                vAccident2.setVisibility(View.VISIBLE);
                break;
            case R.id.lloAccident2:
                tvAccident2.setText(path);
                vAccident3.setVisibility(View.VISIBLE);
                break;
            case R.id.lloAccident3:
                tvAccident3.setText(path);
                vAccident4.setVisibility(View.VISIBLE);
                break;
            case R.id.lloAccident4:
                tvAccident4.setText(path);
                vAccident5.setVisibility(View.VISIBLE);
                break;
            case R.id.lloAccident5:
                tvAccident5.setText(path);
                vAccident6.setVisibility(View.VISIBLE);
                break;
            case R.id.lloAccident6:
                tvAccident6.setText(path);
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sNatureOfLoss:
                selectedNatureOfLoss = getResources().getStringArray(R.array.nature_of_claim_value)[position];
                break;
            case R.id.sPolicyNo:
                selectedPolicyNo = motorPolicyNo[position];
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }


    String Tag_Request = "getMotorPolices";

    //To add claim we need to get the motor policies first.
    private void getMotorPolicy() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetMotorPolicies
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setPoliciesSpinner(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(AddMotorClaimActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getMotorPolicy();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting = new SharedPreferencesClass(AddMotorClaimActivity.this);
                params.put("cpr", setting.getCpr());
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(AddMotorClaimActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setPoliciesSpinner(JSONObject jsonObject) {
        try {
            Log.i("getUserInfo", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                JSONArray resultArray = jsonObject.getJSONArray("result");
                if (resultArray.length() > 0) {
                    motorPolicyNo = new String[resultArray.length()];
                    for (int i = 0; i < resultArray.length(); i++) {
                        motorPolicyNo[i] = resultArray.getJSONObject(i).getString("policyNumber");
                    }
                    sPolicyNo.setAdapter(new SpinnerAdapter(AddMotorClaimActivity.this, motorPolicyNo));
                } else {
                    findViewById(R.id.tvNoMotorPolicy).setVisibility(View.VISIBLE);
                }
            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else {
                MyMethods.showNetworkErrorDialog(AddMotorClaimActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getMotorPolicy();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String policyNo, dateOfLoss, natureOfLoss, timeOfLoss, policyReportNo, policyReportedDate,
            fileCpr, fileCprBack, fileVehicleCertificate, fileVehicleCertificateBack, fileLicense,
            fileLicenseBack, filePolicyReport, fileAccident1, fileAccident2, fileAccident3,
            fileAccident4, fileAccident5, fileAccident6;

    int READ_EXTERNAL = 1;

    private void prepareAddClaim() {
        policyNo = selectedPolicyNo;
        dateOfLoss = etDateOfLoss.getText().toString();
        natureOfLoss = selectedNatureOfLoss;
        timeOfLoss = etTime.getText().toString();
        policyReportNo = etReportNo.getText().toString();
        policyReportedDate = etPolicyReportedDate.getText().toString();
        fileCpr = tvCpr.getText().toString();
        fileCprBack = tvCprBack.getText().toString();
        fileVehicleCertificate = tvVehicleCertificate.getText().toString();
        fileVehicleCertificateBack = tvVehicleCertificateBack.getText().toString();
        fileLicense = tvLicense.getText().toString();
        fileLicenseBack = tvLicenseBack.getText().toString();
        filePolicyReport = tvPolicyReport.getText().toString();
        fileAccident1 = tvAccident1.getText().toString();
        fileAccident2 = tvAccident2.getText().toString();
        fileAccident3 = tvAccident3.getText().toString();
        fileAccident4 = tvAccident4.getText().toString();
        fileAccident5 = tvAccident5.getText().toString();
        fileAccident6 = tvAccident6.getText().toString();

        if (policyNo.isEmpty() || dateOfLoss.isEmpty() || natureOfLoss.isEmpty() ||
                timeOfLoss.isEmpty() || policyReportNo.isEmpty() || policyReportedDate.isEmpty() ||
                fileCpr.isEmpty() || fileCprBack.isEmpty() || fileVehicleCertificate.isEmpty() ||
                fileVehicleCertificateBack.isEmpty() || fileLicense.isEmpty() ||
                fileLicenseBack.isEmpty() || filePolicyReport.isEmpty()) {
            Snackbar.make(view, R.string.message_enterRequiredData, Snackbar.LENGTH_SHORT).show();
        } else {
            //permission required for android 6.0 and above.
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                MyMethods.rationalDialog(this, R.string.dialog_message_permStorage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(AddMotorClaimActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL);
                    }
                });
            } else {
                ActivityCompat.requestPermissions(AddMotorClaimActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL);
            }
        }
    }

    /**
     * This is the result after prepareAddClaim() to check if the app have the permission to read
     * files for uploading them in the server
     *
     * @see #prepareAddClaim()
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_EXTERNAL) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                new UploadFileToServer().execute();

            } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                MyMethods.rationalDialog(this, R.string.dialog_message_permStorage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 2);
                    }
                });
            }
        }
    }


    //We override this and remove super.onSaveInstanceState to remove some bug that will occur when uploading.
    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        AddProgressDialog addProgressDialog;
        int actualProgress = 0, currentProgress = 0;
        long totalSize = 0;

        public UploadFileToServer() {
            addProgressDialog = new AddProgressDialog();
            addProgressDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isUploadFinished = false;
            addProgressDialog.show(getSupportFragmentManager(), "ProgressAddActivity");
            manageUploadProgress();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            actualProgress = progress[0];
        }

        @Override
        protected String doInBackground(Void... params) {
            return addActivity();
        }

        private String addActivity() {
            String responseString;
            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, System.getProperty("http.agent"));

            HttpPost httppost = new HttpPost(APIs.AddClaim);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(HttpMultipartMode.STRICT, null,
                        Charset.forName("utf-8"), new AndroidMultiPartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        publishProgress((int) ((num / (float) totalSize) * 100));
                    }
                });

                entity.addPart("file_vehicleCertificate", new FileBody(getCompressImageFile(fileVehicleCertificate)));
                entity.addPart("file_vehicleCertificate_back", new FileBody(getCompressImageFile(fileVehicleCertificateBack)));
                entity.addPart("file_cpr", new FileBody(getCompressImageFile((fileCpr))));
                entity.addPart("file_cpr_back", new FileBody(getCompressImageFile((fileCprBack))));
                entity.addPart("file_policyReport", new FileBody(getCompressImageFile((filePolicyReport))));
                entity.addPart("file_license", new FileBody(getCompressImageFile((fileLicense))));
                entity.addPart("file_license_back", new FileBody(getCompressImageFile((fileLicenseBack))));

                if (!fileAccident1.isEmpty())
                    entity.addPart("file_accident1", new FileBody(getCompressImageFile((fileAccident1))));
                if (!fileAccident2.isEmpty())
                    entity.addPart("file_accident2", new FileBody(getCompressImageFile((fileAccident2))));
                if (!fileAccident3.isEmpty())
                    entity.addPart("file_accident3", new FileBody(getCompressImageFile((fileAccident3))));
                if (!fileAccident4.isEmpty())
                    entity.addPart("file_accident4", new FileBody(getCompressImageFile((fileAccident4))));
                if (!fileAccident5.isEmpty())
                    entity.addPart("file_accident5", new FileBody(getCompressImageFile((fileAccident5))));
                if (!fileAccident6.isEmpty())
                    entity.addPart("file_accident6", new FileBody(getCompressImageFile((fileAccident6))));

                entity.addPart("policyNo", new StringBody(policyNo));
                entity.addPart("dateOfLoss", new StringBody(dateOfLoss));
                entity.addPart("natureOfLoss", new StringBody(natureOfLoss));
                entity.addPart("timeOfLoss", new StringBody(timeOfLoss));
                entity.addPart("policyReportNo", new StringBody(policyReportNo));
                entity.addPart("policyReportedDate", new StringBody(policyReportedDate));

                entity.addPart("cpr", new StringBody(setting.getCpr()));
                entity.addPart("userId", new StringBody(String.valueOf(setting.getUserId())));
                entity.addPart("token", new StringBody(setting.getAccessToken()));
                entity.addPart("deviceId", new StringBody(MyMethods.getDeviceId(AddMotorClaimActivity.this)));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();
                int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode == 200) {
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }
            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            Thread thread = new Thread() {
                public void run() {
                    while (true) {
                        if (isUploadFinished) {
                            addProgressDialog.dismiss();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    executeResult(result);
                                }
                            });
                            break;
                        }

                        //Make it sleep one second reset on every checking
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            thread.start();
        }

        private void executeResult(String result) {
            try {
                Log.i("executeResult", result);
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.getBoolean("success")) {
                    MyMethods.showGeneralDialogMessage(AddMotorClaimActivity.this,
                            R.string.message_claim_added, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(AddMotorClaimActivity.this, MainActivity.class);
                                    intent.putExtra("isClaimPage", true);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            });
                } else if (jsonObject.getInt("status") == 4) {
                    MyMethods.showLoginPage(AddMotorClaimActivity.this);
                } else {
                    MyMethods.showSnackBarMessage(findViewById(R.id.main), jsonObject.getString(getString(R.string.api_message)));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        boolean isUploadFinished = false;

        private void manageUploadProgress() {
            Thread thUpdateProgress = new Thread() {
                public void run() {
                    while (currentProgress <= 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (actualProgress >= currentProgress)
                                    addProgressDialog.updateProgress(currentProgress++);
                            }
                        });
                        try {
                            Thread.sleep(getSleepTime());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    actualProgress = 0;
                    isUploadFinished = true;
                }
            };
            thUpdateProgress.start();
        }

        double x;
        double sleepTime;

        private int getSleepTime() {
            if (currentProgress != 0) {
                x = (actualProgress - currentProgress);
            }
            sleepTime = 300 / (x == 0 ? 1 : x);
            return (int) (sleepTime < 10 ? 10 : sleepTime);
        }
    }

    private File getCompressImageFile(String imagePath) {
        Bitmap coverImage = MyMethods.decodeFile(new File(imagePath));
        //fix image rotation  if needed
        int rotationNeed = MyMethods.getImageFileOrientation(imagePath);
        if (rotationNeed != 0)
            coverImage = MyMethods.rotateBitmap(coverImage, rotationNeed);

        File file = new File(getCacheDir(), "userId" + setting.getUserId()
                + System.currentTimeMillis() + ".png");
        try {
            file.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            coverImage.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] byteArray = bos.toByteArray();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(byteArray);
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                if (motorPolicyNo != null) {
                    MyMethods.hideKeyboard(this);
                    prepareAddClaim();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
