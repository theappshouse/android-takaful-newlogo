package com.appshouse.takaful.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appshouse.takaful.R;
import com.appshouse.takaful.attributes.Product;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.constant.ProductCodes;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.MyTagHandler;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.squareup.picasso.Picasso;

/**
 * Created by Mohammed Algassab on 2/6/2016.
 */
public class ProductDetailActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView tvProductTitle, tvProductShortDesc, tvProductDesc;
    ImageView ivProduct;
    LinearLayout lloProductGetQuote, lloProductCallMe;
    Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_productDetail);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        tvProductTitle = (TextView) findViewById(R.id.tvProductTitle);
        tvProductShortDesc = (TextView) findViewById(R.id.tvProductShortDesc);
        tvProductDesc = (TextView) findViewById(R.id.tvProductDesc);
        ivProduct = (ImageView) findViewById(R.id.ivProduct);
        lloProductCallMe = (LinearLayout) findViewById(R.id.lloProductCallMe);
        lloProductGetQuote = (LinearLayout) findViewById(R.id.lloProductGetQuote);

        product = getIntent().getParcelableExtra("product");
        setProductDetail();

        lloProductCallMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailActivity.this, CallMeActivity.class);
                intent.putExtra("productName", product.mTitle);
                intent.putExtra("productCode", product.mCode);
                startActivity(intent);
            }
        });
    }

    private void setProductDetail() {
        Picasso.with(this).load(APIs.ImageURL + product.mImagePath).into(ivProduct);
        tvProductTitle.setText(product.mTitle);
        tvProductShortDesc.setText(product.mShotDesc);
        tvProductDesc.setText(Html.fromHtml(product.mDesc,null,new MyTagHandler()));
        tvProductDesc.setMovementMethod(LinkMovementMethod.getInstance());

        if (product.mHasQuote) {
            lloProductGetQuote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!new SharedPreferencesClass(ProductDetailActivity.this).isLogin()) {
                        Intent intent = new Intent(ProductDetailActivity.this, QuoteUserInfoActivity.class);
                        intent.putExtra("productCode", product.mCode);
                        startActivity(intent);
                    } else {
                        switch (product.mCode) {
                            case ProductCodes.MOTOR:
                                startActivity(new Intent(ProductDetailActivity.this, QuoteMotorActivity.class));
                                break;

                            case ProductCodes.TRAVEL:
                                startActivity(new Intent(ProductDetailActivity.this, QuoteTravelActivity.class));
                                break;

                            case ProductCodes.FIRE:
                                startActivity(new Intent(ProductDetailActivity.this, QuoteFireActivity.class));
                                break;

                            case ProductCodes.BAITAK:
                                startActivity(new Intent(ProductDetailActivity.this, QuoteBaitakActivity.class));
                                break;

                            default:
                                Toast.makeText(ProductDetailActivity.this, getString(R.string.not_available), Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                }
            });
        } else {
            lloProductGetQuote.setClickable(false);
            lloProductGetQuote.setBackgroundColor(getResources().getColor(R.color.disable_transparent_button));
        }
    }
}
