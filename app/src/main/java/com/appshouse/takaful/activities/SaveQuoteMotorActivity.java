package com.appshouse.takaful.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appshouse.takaful.BuildConfig;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.dialogs.AddProgressDialog;
import com.appshouse.takaful.utilities.AndroidMultiPartEntity;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.nononsenseapps.filepicker.AbstractFilePickerActivity;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.splunk.mint.Mint;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;

/**
 * Created by Mohammed Algassab on 2/4/2016.
 */
public class SaveQuoteMotorActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    double premium, sumAssured;
    TextView tvPremium, tvSumAssured, tvNcbPath, tvCprPath, tvCprBackPath, tvLicensePath, tvLicenseBackPath,
            tvVehicleOwnershipPath, tvVehicleOwnershipBackPath, tvLastInsurancePath;
    LinearLayout lloFileNcb, lloFileCpr, lloFileCprBack, lloFileLicense, lloFileLicenseBack,
            lloFileVehicleOwnership, lloFileVehicleOwnershipBack, lloLastInsurance;
    SharedPreferencesClass setting;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_motor_quote);

        // test failures answer
        //StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        //StrictMode.setVmPolicy(newbuilder.build());

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_saveMotorQuote);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setting = new SharedPreferencesClass(this);

        tvPremium = (TextView) findViewById(R.id.tvPremium);
        tvSumAssured = (TextView) findViewById(R.id.tvSumAssured);
        tvNcbPath = (TextView) findViewById(R.id.tvNcbPath);
        tvCprPath = (TextView) findViewById(R.id.tvCprPath);
        tvCprBackPath = (TextView) findViewById(R.id.tvCprBackPath);
        tvLicensePath = (TextView) findViewById(R.id.tvLicensePath);
        tvLicenseBackPath = (TextView) findViewById(R.id.tvLicenseBackPath);
        tvVehicleOwnershipPath = (TextView) findViewById(R.id.tvVehicleOwnershipPath);
        tvVehicleOwnershipBackPath = (TextView) findViewById(R.id.tvVehicleOwnershipBackPath);
        tvLastInsurancePath = (TextView) findViewById(R.id.tvLastInsurancePath);

        lloFileNcb = (LinearLayout) findViewById(R.id.lloFileNcb);
        lloFileCpr = (LinearLayout) findViewById(R.id.lloFileCpr);
        lloFileCprBack = (LinearLayout) findViewById(R.id.lloFileCprBack);
        lloFileLicense = (LinearLayout) findViewById(R.id.lloFileLicense);
        lloFileLicenseBack = (LinearLayout) findViewById(R.id.lloFileLicenseBack);
        lloFileVehicleOwnership = (LinearLayout) findViewById(R.id.lloFileVehicleOwnership);
        lloFileVehicleOwnershipBack = (LinearLayout) findViewById(R.id.lloFileVehicleOwnershipBack);
        lloLastInsurance = (LinearLayout) findViewById(R.id.lloLastInsurance);

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        premium = getIntent().getDoubleExtra("premium", 0);
        sumAssured = getIntent().getDoubleExtra("sumAssured", 0);
        tvPremium.setText(getString(R.string.bd) + String.valueOf(premium));
        tvSumAssured.setText(getString(R.string.bd) + String.valueOf(sumAssured));

        lloFileNcb.setOnClickListener(this);
        lloFileCpr.setOnClickListener(this);
        lloFileCprBack.setOnClickListener(this);
        lloFileLicense.setOnClickListener(this);
        lloFileLicenseBack.setOnClickListener(this);
        lloFileVehicleOwnership.setOnClickListener(this);
        lloFileVehicleOwnershipBack.setOnClickListener(this);
        lloLastInsurance.setOnClickListener(this);
    }

    /**
     * fileTypeId for getting file path .. we get the id so we know for what we are getting the
     * file path in the setFilePath method
     *
     * @see #setFilePath(String)
     */
    int fileTypeId;

    @Override
    public void onClick(View v) {
        fileTypeId = v.getId();
        uploadFileDialog();
    }

    int FILE_CHOOSER = 0, GalleryAttach_Request = 1, CameraAttach_Request = 2;

    private void uploadFileDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.dialog_title_uploadFile));
        dialog.setMessage(getString(R.string.dialog_message_uploadFile));

        dialog.setPositiveButton(getString(R.string.dialog_button_gallery), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, GalleryAttach_Request);
            }
        });

        dialog.setNeutralButton(getString(R.string.dialog_button_camera), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(cameraIntent.resolveActivity(getPackageManager()) != null){
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri());
                    cameraIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(cameraIntent, CameraAttach_Request);
                }else{
                    Log.d("TESTING","Null");
                }

            }
        });

//        dialog.setNegativeButton(getString(R.string.dialog_button_file), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                openFileChooser();
//            }
//        });
        dialog.show();
    }

    private void openFileChooser() {
        Intent i = new Intent(this, FilePickerActivity.class);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        } else {
            i.putExtra(FilePickerActivity.EXTRA_ALLOW_MULTIPLE, false);
        }
        i.putExtra(FilePickerActivity.EXTRA_ALLOW_CREATE_DIR, false);
        i.putExtra(FilePickerActivity.EXTRA_MODE, FilePickerActivity.MODE_FILE);
        startActivityForResult(i, AbstractFilePickerActivity.MODE_FILE);
    }

    Uri imageUri;

    private Uri setImageUri() {
        File file = new File(Environment.getExternalStorageDirectory()
                + "/DCIM/", "image" + new Date().getTime() + ".png");
        //imageUri = Uri.fromFile(file);
        imageUri = FileProvider.getUriForFile(SaveQuoteMotorActivity.this, BuildConfig.APPLICATION_ID + ".provider",file);
        return imageUri;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == FILE_CHOOSER) {
                Uri uri = data.getData();
                setFilePath(uri.getPath());
            } else if (requestCode == GalleryAttach_Request || requestCode == CameraAttach_Request) {

              //  if (requestCode == GalleryAttach_Request) {
                if (requestCode == GalleryAttach_Request) {
                    imageUri = getImageUriFromCamera(data.getData());
                } else if (requestCode != CameraAttach_Request) {
                    imageUri = null;
                }
                if (imageUri != null) {
                    setFilePath(imageUri.getPath());
                }
            }
        }
    }

    private Uri getImageUriFromCamera(Uri uri) {
        String[] fileColumn = {MediaStore.Images.Media.DATA};
        Cursor imageCursor = getContentResolver().query(uri, fileColumn, null, null, null);
        imageCursor.moveToFirst();
        int fileColumnIndex = imageCursor.getColumnIndex(fileColumn[0]);
        return Uri.parse(imageCursor.getString(fileColumnIndex));
    }

    private void setFilePath(String path) {
        switch (fileTypeId) {
            case R.id.lloFileNcb:
                tvNcbPath.setText(path);
                break;
            case R.id.lloFileCpr:
                tvCprPath.setText(path);
                break;
            case R.id.lloFileCprBack:
                tvCprBackPath.setText(path);
                break;
            case R.id.lloFileLicense:
                tvLicensePath.setText(path);
                break;
            case R.id.lloFileLicenseBack:
                tvLicenseBackPath.setText(path);
                break;
            case R.id.lloFileVehicleOwnership:
                tvVehicleOwnershipPath.setText(path);
                break;
            case R.id.lloFileVehicleOwnershipBack:
                tvVehicleOwnershipBackPath.setText(path);
                break;
            case R.id.lloLastInsurance:
                tvLastInsurancePath.setText(path);
                break;
        }
    }

    String ncbPath, cprPath, cprPathBack, licensePath, licensePathBack, vehiclePath, vehiclePathBack, lastInsurance;
    int READ_EXTERNAL = 1;

    private void prepareSaveQuote() {
        ncbPath = tvNcbPath.getText().toString();
        cprPath = tvCprPath.getText().toString();
        cprPathBack = tvCprBackPath.getText().toString();
        licensePath = tvLicensePath.getText().toString();
        licensePathBack = tvLicenseBackPath.getText().toString();
        vehiclePath = tvVehicleOwnershipPath.getText().toString();
        vehiclePathBack = tvVehicleOwnershipBackPath.getText().toString();
        lastInsurance = tvLastInsurancePath.getText().toString();

        if ((ncbPath.isEmpty() && !getIntent().getStringExtra("ncb").contentEquals("A")) || cprPath.isEmpty() || cprPathBack.isEmpty()
                || licensePath.isEmpty() || licensePathBack.isEmpty() || vehiclePath.isEmpty() || vehiclePathBack.isEmpty()) {
            Snackbar.make(findViewById(R.id.main), R.string.message_enterRequiredData, Snackbar.LENGTH_LONG).show();
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                MyMethods.rationalDialog(this, R.string.dialog_message_permStorage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(SaveQuoteMotorActivity.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL);
                    }
                });
            } else {
                ActivityCompat.requestPermissions(SaveQuoteMotorActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_EXTERNAL) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                new UploadFileToServer().execute();

            } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                MyMethods.rationalDialog(this, R.string.dialog_message_permStorage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 2);
                    }
                });
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
    }

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        AddProgressDialog addProgressDialog;
        int actualProgress = 0, currentProgress = 0;
        long totalSize = 0;

        public UploadFileToServer() {
            addProgressDialog = new AddProgressDialog();
            addProgressDialog.setCancelable(false);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isUploadFinished = false;
            addProgressDialog.show(getSupportFragmentManager(), "ProgressAddActivity");
            manageUploadProgress();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            actualProgress = progress[0];
        }

        @Override
        protected String doInBackground(Void... params) {
            return addActivity();
        }

        private String addActivity() {
            String responseString;
            HttpClient httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.USER_AGENT, System.getProperty("http.agent"));

            HttpPost httppost = new HttpPost(APIs.GetSaveMotorQuote);

            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(HttpMultipartMode.STRICT, null,
                        Charset.forName("utf-8"), new AndroidMultiPartEntity.ProgressListener() {
                    @Override
                    public void transferred(long num) {
                        publishProgress((int) ((num / (float) totalSize) * 100));
                    }
                });

                if (!ncbPath.isEmpty())
                    entity.addPart("file_ncb", new FileBody(getCompressImageFile(ncbPath)));
                entity.addPart("file_cpr", new FileBody(getCompressImageFile(cprPath)));
                entity.addPart("file_cpr_back", new FileBody(getCompressImageFile(cprPathBack)));
                entity.addPart("file_license", new FileBody(getCompressImageFile(licensePath)));
                entity.addPart("file_license_back", new FileBody(getCompressImageFile(licensePathBack)));
                entity.addPart("file_vehicle", new FileBody(getCompressImageFile(vehiclePath)));
                entity.addPart("file_vehicle_back", new FileBody(getCompressImageFile(vehiclePathBack)));

                if (!lastInsurance.isEmpty())
                    entity.addPart("file_last_insurance", new FileBody(getCompressImageFile(lastInsurance)));

                //to accept arabic language
                Charset chars = Charset.forName("UTF-8");
                Intent i = getIntent();
                entity.addPart("policyStartDate", new StringBody(i.getStringExtra("policyStartDate")));
                entity.addPart("noYearLicense", new StringBody(i.getStringExtra("noYearLicense")));
                entity.addPart("regNo", new StringBody(i.getStringExtra("regNo")));
                entity.addPart("chassisNo", new StringBody(i.getStringExtra("chassisNo")));
                entity.addPart("vehicleType", new StringBody(i.getStringExtra("vehicleType")));
                entity.addPart("bodyType", new StringBody(i.getStringExtra("bodyType")));
                entity.addPart("make", new StringBody(i.getStringExtra("make")));
                entity.addPart("modelType", new StringBody(i.getStringExtra("modelType")));
                entity.addPart("model", new StringBody(i.getStringExtra("model")));
                entity.addPart("ccHp", new StringBody(i.getStringExtra("ccHp")));
                entity.addPart("cylindersNo", new StringBody(i.getStringExtra("cylindersNo")));
                entity.addPart("seatingNo", new StringBody(i.getStringExtra("seatingNo")));
                entity.addPart("regMonth", new StringBody(i.getStringExtra("regMonth")));
                entity.addPart("vehicleReplacmentDays", new StringBody(i.getStringExtra("vehicleReplacmentDays")));
                entity.addPart("vehicleValue", new StringBody(i.getStringExtra("vehicleValue")));
                entity.addPart("ncb", new StringBody(i.getStringExtra("ncb")));
                entity.addPart("coverCode", new StringBody(i.getStringExtra("coverCode")));
                entity.addPart("cpr", new StringBody(i.getStringExtra("cpr")));
                entity.addPart("addCover", new StringBody(i.getStringExtra("addCover")));
                entity.addPart("saveQuote", new StringBody("1"));
                entity.addPart("premium", new StringBody(String.valueOf(premium)));
                entity.addPart("userId", new StringBody(String.valueOf(setting.getUserId())));
                entity.addPart("deviceId", new StringBody(MyMethods.getDeviceId(SaveQuoteMotorActivity.this)));
                entity.addPart("token", new StringBody(setting.getAccessToken()));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();
                int statusCode = response.getStatusLine().getStatusCode();

                if (statusCode == 200) {
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: " + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }
            return responseString;
        }

        @Override
        protected void onPostExecute(final String result) {
            super.onPostExecute(result);
            Thread thread = new Thread() {
                public void run() {
                    while (true) {
                        if (isUploadFinished) {
                            addProgressDialog.dismiss();
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    executeResult(result);
                                }
                            });
                            break;
                        }

                        //Make it sleep one second reset on every checking
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            thread.start();
        }

        private void executeResult(String result) {
            try {
                Log.i("executeResult", result);
                JSONObject jsonObject = new JSONObject(result);
                if (jsonObject.getBoolean("success")) {
                    Mint.transactionStop("Motor Policy");
                    MyMethods.showGeneralDialogMessage(SaveQuoteMotorActivity.this,
                            R.string.message_quoteSavedSuccess2, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(SaveQuoteMotorActivity.this, MainActivity.class);
                                    intent.putExtra("isMyTakafulPage", true);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            });
                } else if (jsonObject.getInt("status") == 4) {
                    MyMethods.showLoginPage(SaveQuoteMotorActivity.this);
                } else {
                    MyMethods.showSnackBarMessage(findViewById(R.id.main), jsonObject.getString(getString(R.string.api_message)));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        boolean isUploadFinished = false;

        private void manageUploadProgress() {
            Thread thUpdateProgress = new Thread() {
                public void run() {
                    while (currentProgress <= 100) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (actualProgress >= currentProgress)
                                    addProgressDialog.updateProgress(currentProgress++);
                            }
                        });
                        try {
                            Thread.sleep(getSleepTime());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    actualProgress = 0;
                    isUploadFinished = true;
                }
            };
            thUpdateProgress.start();
        }

        double x;
        double sleepTime;

        private int getSleepTime() {
            if (currentProgress != 0) {
                x = (actualProgress - currentProgress);
            }
            sleepTime = 300 / (x == 0 ? 1 : x);
            return (int) (sleepTime < 10 ? 10 : sleepTime);
        }
    }

    private File getCompressImageFile(String imagePath) {
        Bitmap coverImage = MyMethods.decodeFile(new File(imagePath));
        //fix image rotation  if needed
        int rotationNeed = MyMethods.getImageFileOrientation(imagePath);
        if (rotationNeed != 0)
            coverImage = MyMethods.rotateBitmap(coverImage, rotationNeed);

        File file = new File(getCacheDir(), "userId" + setting.getUserId()
                + System.currentTimeMillis() + ".png");
        try {
            file.createNewFile();
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            coverImage.compress(Bitmap.CompressFormat.PNG, 0, bos);
            byte[] byteArray = bos.toByteArray();
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(byteArray);
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                MyMethods.hideKeyboard(this);
                prepareSaveQuote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}

