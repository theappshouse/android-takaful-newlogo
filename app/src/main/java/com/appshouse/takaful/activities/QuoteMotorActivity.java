package com.appshouse.takaful.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.SpinnerAdapter;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.InputFilterMinMax;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.splunk.mint.Mint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/31/2016.
 */
public class QuoteMotorActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, AdapterView.OnItemSelectedListener, View.OnKeyListener {
    Toolbar toolbar;
    EditText etPolicyStartDay, etYearNoLicense, etRegistrationNo, etChassisNo, etModel, etCcHp, etCylinderNo,
            etSeatingNo, etVehicleValue, etExcess;
    LinearLayout lloPolicyStartPickDate;
    InputMethodManager inputMethodManager;


    private class VehicleType {
        public String mName;
        public String mValue;
        public ArrayList<BodyType> mBodyTypes;

        public VehicleType(String name, String valeu) {
            mName = name;
            mValue = valeu;
        }
    }

    private class BodyType {
        public int mId;
        public String mCode;
        public String mName;

        public BodyType(int id, String code, String name) {
            mId = id;
            mCode = code;
            mName = name;
        }
    }

    private class VehicleMake {
        public int mId;
        public String mName;
        ArrayList<VehicleModel> mVehicleModels;

        public VehicleMake(int id, String name) {
            mId = id;
            mName = name;
        }
    }

    private class VehicleModel {
        public int mId;
        public int mMakeId;
        public String mName;
        public String mCode;

        public VehicleModel(int id, int makeId, String name, String code) {
            mId = id;
            mMakeId = makeId;
            mName = name;
            mCode = code;
        }
    }

    //The below is related to spinners
    Spinner sVehicleType, sBodyType, sVehicleMake, sVehicleModel, sRegistrationMonth, sNCB, sVehicleRepDays;
    String[] selectedBodyTypeNames, vehicleMakeNames, selectedVehicleModelName, vehicleTypeNames;

    ArrayList<VehicleType> vehicleTypes;
    ArrayList<VehicleMake> vehicleMakes;
    ArrayList<BodyType> selectedBodyTypeList;
    ArrayList<VehicleModel> selectedVehicleModelList;

    VehicleType selectedVehicleType;
    BodyType selectedBodyType;
    VehicleMake selectedVehicleMake;
    VehicleModel selectedVehicleModel;
    String selectedRegMonthValue, selectedNcbValue = "", selectedVehicleRepDays = "0";

    //The below code related to checkbox and radio buttons
    RadioButton rdThirdParty, rdCompPlus, rdCompSilver, rdCompGold;
    CheckBox cbRiotStrike, cbPersonalAccident, cbRoadAssist, cbStormFlood, cbWindScreenCoverage;

    String selectedCoverValue = "";
    SharedPreferencesClass setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mint.transactionStart(getString(R.string.title_motor_policy));

        setContentView(R.layout.activity_quote_motor);
        setting = new SharedPreferencesClass(this);
        inputMethodManager = ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE));

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_getMotorQuote);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        etPolicyStartDay = (EditText) findViewById(R.id.etPolicyStartDay);
        etYearNoLicense = (EditText) findViewById(R.id.etYearNoLicense);
        etRegistrationNo = (EditText) findViewById(R.id.etRegistrationNo);
        etChassisNo = (EditText) findViewById(R.id.etChassisNo);
        etModel = (EditText) findViewById(R.id.etModel);
        etCcHp = (EditText) findViewById(R.id.etCcHp);
        etCylinderNo = (EditText) findViewById(R.id.etCylinderNo);
        etSeatingNo = (EditText) findViewById(R.id.etSeatingNo);
        etVehicleValue = (EditText) findViewById(R.id.etVehicleValue);
        etExcess = (EditText) findViewById(R.id.etExcess);

        etVehicleValue.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!etVehicleValue.getText().toString().isEmpty()) {
                    double value = Double.parseDouble(etVehicleValue.getText().toString());
                    if (value <= 9999) {
                        etExcess.setText("50 "+ getString(R.string.bd));
                    } else if (value >= 10000 && value <= 19999) {
                        etExcess.setText("100 "+ getString(R.string.bd));
                    } else if (value >= 20000 && value <= 30000) {
                        etExcess.setText("250 "+ getString(R.string.bd));
                    } else {
                        etExcess.setText(R.string.excess_more3000);
                    }
                } else {
                    etExcess.setText("");
                }
            }
        });

        etModel.setFilters(new InputFilter[]{new InputFilterMinMax(0, Calendar.getInstance().get(Calendar.YEAR) + 1)});

        sVehicleType = (Spinner) findViewById(R.id.sVehicleType);
        sBodyType = (Spinner) findViewById(R.id.sBodyType);
        sVehicleMake = (Spinner) findViewById(R.id.sVehicleMake);
        sVehicleModel = (Spinner) findViewById(R.id.sVehicleModel);
        sRegistrationMonth = (Spinner) findViewById(R.id.sRegistrationMonth);
        sNCB = (Spinner) findViewById(R.id.sNCB);
        sVehicleRepDays = (Spinner) findViewById(R.id.sVehicleRepDays);

        etChassisNo.setOnKeyListener(this);

        rdThirdParty = (RadioButton) findViewById(R.id.rdThirdParty);
        rdCompPlus = (RadioButton) findViewById(R.id.rdCompPlus);
        rdCompSilver = (RadioButton) findViewById(R.id.rdCompSilver);
        rdCompGold = (RadioButton) findViewById(R.id.rdCompGold);

        cbRiotStrike = (CheckBox) findViewById(R.id.cbRiotStrike);
        cbPersonalAccident = (CheckBox) findViewById(R.id.cbPersonalAccident);
        cbRoadAssist = (CheckBox) findViewById(R.id.cbRoadAssist);
        cbStormFlood = (CheckBox) findViewById(R.id.cbStormFlood);
        cbWindScreenCoverage = (CheckBox) findViewById(R.id.cbWindScreenCoverage);

        lloPolicyStartPickDate = (LinearLayout) findViewById(R.id.lloPolicyStartPickDate);

        rdThirdParty.setOnCheckedChangeListener(this);
        rdCompPlus.setOnCheckedChangeListener(this);
        rdCompSilver.setOnCheckedChangeListener(this);
        rdCompGold.setOnCheckedChangeListener(this);

        sBodyType.setOnItemSelectedListener(this);
        sVehicleType.setOnItemSelectedListener(this);
        sVehicleMake.setOnItemSelectedListener(this);
        sVehicleModel.setOnItemSelectedListener(this);
        sRegistrationMonth.setOnItemSelectedListener(this);
        sNCB.setOnItemSelectedListener(this);
        sVehicleRepDays.setOnItemSelectedListener(this);

        lloPolicyStartPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPolicyStartDay.requestFocus();
                openPickDateDialog();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        rdThirdParty.setChecked(true);
        getOptionData();
        //setAutoDemoData();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
            inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
            switch (v.getId()) {
                case R.id.etChassisNo:
                    etChassisNo.clearFocus();
                    sVehicleType.setFocusable(true);
                    sVehicleType.setFocusableInTouchMode(true);
                    sVehicleType.requestFocus();
                    break;
                case R.id.etSeatingNo:
                    etSeatingNo.clearFocus();
                    sRegistrationMonth.setFocusable(true);
                    sRegistrationMonth.setFocusableInTouchMode(true);
                    sRegistrationMonth.requestFocus();
                    break;
                case R.id.etVehicleValue:
                    etVehicleValue.clearFocus();
                    sNCB.setFocusable(true);
                    sNCB.setFocusableInTouchMode(true);
                    sNCB.requestFocus();
                    break;
            }
            // Perform action on Enter key press
            return true;
        }
        return false;
    }

    private void setAutoDemoData() {
        etPolicyStartDay.setText("02/02/2016");
        etYearNoLicense.setText("5");
        etRegistrationNo.setText("6test3452");
        etChassisNo.setText("");
        etModel.setText("2012");
        etCcHp.setText("1400");
        etCylinderNo.setText("5");
        etSeatingNo.setText("5");
        etVehicleValue.setText("10");
    }

    private void openPickDateDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mYear, mMonth, mDay;
                mYear = String.valueOf(year);
                mMonth = String.valueOf(monthOfYear + 1);
                mDay = String.valueOf(dayOfMonth);

//              etPolicyStartDay.setText(mDay + "/" + mMonth + "/" + mYear);
                String date = mDay + "/" + mMonth + "/" + mYear;
                long dateTime = MyMethods.dateToSecond(date, "dd/MM/yyyy");
                if ((System.currentTimeMillis() / 1000) < (dateTime + 86400))
                    etPolicyStartDay.setText(date);
                else {
                    Snackbar.make(findViewById(R.id.main), R.string.message_invalidDate, Snackbar.LENGTH_LONG).show();
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sVehicleType:
                selectedVehicleType = vehicleTypes.get(position);
                setTypeOfBody();
                break;

            case R.id.sBodyType:
                selectedBodyType = selectedBodyTypeList.get(position);
                break;

            case R.id.sVehicleMake:
                selectedVehicleMake = vehicleMakes.get(position);
                setVehicleModel();
                break;

            case R.id.sVehicleModel:
                selectedVehicleModel = selectedVehicleModelList.get(position);
                break;

            case R.id.sRegistrationMonth:
                selectedRegMonthValue = String.valueOf(position + 1);
                break;

            case R.id.sNCB:
                selectedNcbValue = getResources().getStringArray(R.array.ncb_value)[position];
                //Toast.makeText(this,selectedNcbValue,Toast.LENGTH_SHORT).show();
                //setCoverSelection();
                break;

            case R.id.sVehicleRepDays:
                selectedVehicleRepDays = getResources().getStringArray(R.array.vehicle_rep_day_value)[position];
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void setTypeOfBody() {
        selectedBodyTypeList = selectedVehicleType.mBodyTypes;
        selectedBodyTypeNames = new String[selectedBodyTypeList.size()];
        for (int i = 0; i < selectedBodyTypeList.size(); i++) {

            //some joke here from our customer
            if (selectedVehicleType.mValue.contentEquals(getResources().getStringArray(R.array.vehicle_type_value)[0])) {
                selectedBodyTypeNames = new String[1];
                selectedBodyTypeNames[i] = selectedBodyTypeList.get(i).mName;
                break;
            }

            selectedBodyTypeNames[i] = selectedBodyTypeList.get(i).mName;
        }

        sBodyType.setAdapter(new SpinnerAdapter(this, selectedBodyTypeNames));
    }

    private void setVehicleModel() {
        selectedVehicleModelList = selectedVehicleMake.mVehicleModels;
        selectedVehicleModelName = new String[selectedVehicleModelList.size()];
        for (int i = 0; i < selectedVehicleModelList.size(); i++)
            selectedVehicleModelName[i] = selectedVehicleModelList.get(i).mName;

        sVehicleModel.setAdapter(new SpinnerAdapter(this, selectedVehicleModelName));
    }

//    private void setCoverSelection() {
//        if (selectedNcbValue.contentEquals("A")) {
//            rdThirdParty.setEnabled(true);
//        } else {
//            rdThirdParty.setEnabled(false);
//            if (rdThirdParty.isChecked()) {
//                rdThirdParty.setChecked(false);
//                rdCompPlus.setChecked(true);
//            }
//        }
//    }


    private void getOptionData() {
        String Tag_Request = "GetOptionData";
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.setCancelable(false);
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetStartDataForMotorQuote
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                setOptionData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                MyMethods.showNetworkErrorDialog(QuoteMotorActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getOptionData();
                    }
                });
            }
        });
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setOptionData(JSONObject jsonObject) {
        try {
            if (jsonObject.getBoolean("success")) {
                JSONObject resultObject = jsonObject.getJSONObject("result");

                //TODO: the code below should be done in other way
                vehicleTypes = new ArrayList<>();
                vehicleTypeNames = getResources().getStringArray(R.array.vehicle_type);
                String[] vehicleTypeValues = getResources().getStringArray(R.array.vehicle_type_value);
                JSONArray bodyArray = resultObject.getJSONArray("bodyTypes");
                for (int i = 0; i < vehicleTypeNames.length; i++) {
                    VehicleType vehicleType = new VehicleType(vehicleTypeNames[i], vehicleTypeValues[i]);

                    ArrayList<BodyType> bodyTypes = new ArrayList<>();
                    for (int j = 0; j < bodyArray.length(); j++) {
                        JSONObject bodyObject = bodyArray.getJSONObject(j);
                        if (i == 0) {
                            //then it is car
                            if (j != 5)
                                //all the values are car except the 5th one
                                bodyTypes.add(new BodyType(bodyObject.getInt("Id"), bodyObject.getString("Code"), bodyObject.getString(getString(R.string.z_body_type_name))));
                        } else if (i == 1) {
                            //then it is motorcycle
                            if (j == 5)
                                //The 5th one is the only motorcycle
                                bodyTypes.add(new BodyType(bodyObject.getInt("Id"), bodyObject.getString("Code"),bodyObject.getString(getString(R.string.z_body_type_name))));
                        }
                    }
                    vehicleType.mBodyTypes = bodyTypes;
                    vehicleTypes.add(vehicleType);
                }

                JSONArray vehicleMakeArray = resultObject.getJSONArray("vehicleMakes");
                JSONArray vehicleModelArray = resultObject.getJSONArray("vehicleModels");
                vehicleMakeNames = new String[vehicleMakeArray.length()];
                vehicleMakes = new ArrayList<>();
                for (int i = 0; i < vehicleMakeArray.length(); i++) {
                    JSONObject vehicleMakeObject = vehicleMakeArray.getJSONObject(i);
                    VehicleMake vehicleMake = new VehicleMake(vehicleMakeObject.getInt("Id"), vehicleMakeObject.getString("Name"));

                    //add the models for the vehicleMake
                    ArrayList<VehicleModel> vehicleModels = new ArrayList<>();
                    for (int j = 0; j < vehicleModelArray.length(); j++) {
                        JSONObject vehicleModelObject = vehicleModelArray.getJSONObject(j);
                        if (vehicleMake.mId == vehicleModelObject.getInt("Make_Id")) {
                            vehicleModels.add(new VehicleModel(vehicleModelObject.getInt("Id"), vehicleModelObject.getInt("Make_Id")
                                    , vehicleModelObject.getString("Model_Name"), vehicleModelObject.getString("Model_Code")));
                        }
                    }
                    vehicleMake.mVehicleModels = vehicleModels;

                    vehicleMakes.add(vehicleMake);
                    vehicleMakeNames[i] = vehicleMake.mName;
                }

                setSpinnerData();
            } else {
                MyMethods.showNetworkErrorDialog(QuoteMotorActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getOptionData();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void setSpinnerData() {
        sVehicleType.setAdapter(new SpinnerAdapter(this, vehicleTypeNames));
        sVehicleMake.setAdapter(new SpinnerAdapter(this, vehicleMakeNames));

        //These values already we got it locally
        sRegistrationMonth.setAdapter(new SpinnerAdapter(this, getResources().getStringArray(R.array.month)));
        sNCB.setAdapter(new SpinnerAdapter(this, getResources().getStringArray(R.array.ncb)));
        sVehicleRepDays.setAdapter(new SpinnerAdapter(this, getResources().getStringArray(R.array.vehicle_rep_day)));
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.isChecked()) {
            disableAllCheckBox();
            switch (buttonView.getId()) {
                //RadioButtons
                case R.id.rdThirdParty:
                    selectedCoverValue = "TP";
                    cbRoadAssist.setEnabled(true);
                    findViewById(R.id.lloVehicleRepDays).setVisibility(View.GONE);
                    findViewById(R.id.lloNcb).setVisibility(View.GONE);
                    findViewById(R.id.lloExcess).setVisibility(View.GONE);
                    sNCB.setSelection(0);
                    break;

                case R.id.rdCompPlus:
                    selectedCoverValue = "CPLUS";
                    cbPersonalAccident.setEnabled(true);
                    cbRiotStrike.setEnabled(true);
                    cbStormFlood.setEnabled(true);
                    cbWindScreenCoverage.setEnabled(true);
                    cbRoadAssist.setChecked(true);
                    break;

                case R.id.rdCompSilver:
                    selectedCoverValue = "CSILV";
                    cbWindScreenCoverage.setEnabled(true);
                    cbPersonalAccident.setChecked(true);
                    cbRiotStrike.setChecked(true);
                    cbStormFlood.setChecked(true);
                    cbRoadAssist.setChecked(true);
                    break;

                case R.id.rdCompGold:
                    selectedCoverValue = "CGOLD";
                    cbWindScreenCoverage.setEnabled(true);
                    cbPersonalAccident.setChecked(true);
                    cbRiotStrike.setChecked(true);
                    cbStormFlood.setChecked(true);
                    cbRoadAssist.setChecked(true);
                    break;
            }
        }
    }

    private void disableAllCheckBox() {
        findViewById(R.id.lloVehicleRepDays).setVisibility(View.VISIBLE);
        findViewById(R.id.lloNcb).setVisibility(View.VISIBLE);
        findViewById(R.id.lloExcess).setVisibility(View.VISIBLE);

        cbRiotStrike.setEnabled(false);
        cbPersonalAccident.setEnabled(false);
        cbRoadAssist.setEnabled(false);
        cbStormFlood.setEnabled(false);
        cbWindScreenCoverage.setEnabled(false);

        cbRiotStrike.setChecked(false);
        cbPersonalAccident.setChecked(false);
        cbRoadAssist.setChecked(false);
        cbStormFlood.setChecked(false);
        cbWindScreenCoverage.setChecked(false);
    }

    String policyStartDate, noYearLicense, regNo, chassisNo, vehicleType, bodyType, make, modelType, cpr,
            model, ccHp, cylindersNo, seatingNo, regMonth, vehicleReplacmentDays, vehicleValue, ncb, coverCode, addCovers;

    private void prepareSendQuote() {
        //get all the details in string
        if (setting.isLogin())
            cpr = setting.getCpr();
        else
            cpr = getIntent().getStringExtra("cpr");

        policyStartDate = etPolicyStartDay.getText().toString();
        noYearLicense = etYearNoLicense.getText().toString();
        regNo = etRegistrationNo.getText().toString();
        chassisNo = etChassisNo.getText().toString();
        vehicleType = selectedVehicleType.mValue;
        bodyType = selectedBodyType.mCode;
        make = String.valueOf(selectedVehicleModel.mName);
        modelType = selectedVehicleModel.mCode;
        model = etModel.getText().toString();
        ccHp = etCcHp.getText().toString();
        cylindersNo = etCylinderNo.getText().toString();
        seatingNo = etSeatingNo.getText().toString();
        regMonth = selectedRegMonthValue;
        vehicleReplacmentDays = selectedVehicleRepDays;
        vehicleValue = etVehicleValue.getText().toString();
        ncb = selectedNcbValue;
        // Toast.makeText(this,"selectedNcbValue:"+selectedNcbValue + " - ncb:" + ncb,Toast.LENGTH_SHORT).show();
        coverCode = selectedCoverValue;

        addCovers = "";
        if (cbPersonalAccident.isChecked())
            addCovers += ",PAB";
        if (cbRiotStrike.isChecked())
            addCovers += ",RS";
        if (cbRoadAssist.isChecked())
            addCovers += ",RA";
        if (cbStormFlood.isChecked())
            addCovers += ",SF";
        if (cbWindScreenCoverage.isChecked())
            addCovers += ",WS";
        if (!addCovers.isEmpty())
            addCovers = addCovers.substring(1);


        if (policyStartDate.isEmpty() || noYearLicense.isEmpty() || (regNo.isEmpty() && chassisNo.isEmpty())
                || model.isEmpty() || ccHp.isEmpty() || cylindersNo.isEmpty() || seatingNo.isEmpty()
                || vehicleReplacmentDays.isEmpty() || vehicleValue.isEmpty()
                || cpr.isEmpty()) {
            Snackbar.make(findViewById(R.id.main), R.string.message_enterRequiredData, Snackbar.LENGTH_LONG).show();
        } else {

            if (model.length() == 4 && Integer.valueOf(model) <= Calendar.getInstance().get(Calendar.YEAR) + 1)
                sendQuote();
            else
                Snackbar.make(findViewById(R.id.main), R.string.message_invalidModelYear, Snackbar.LENGTH_LONG).show();
        }
    }


    private void sendQuote() {
        String Tag_Request = "send quote";
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetSaveMotorQuote
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getSendQuoteResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                sendQuote();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                Log.i("policyStartDate", policyStartDate);
                Log.i("noYearLicense", noYearLicense);
                Log.i("regNo", regNo);
                Log.i("chassisNo", chassisNo);
                Log.i("vehicleType", vehicleType);
                Log.i("bodyType", bodyType);
                Log.i("make", make);
                Log.i("modelType", modelType);
                Log.i("model", model);
                Log.i("ccHp", ccHp);
                Log.i("cylinderNo", cylindersNo);
                Log.i("seatingNo", seatingNo);
                Log.i("regMonth", regMonth);
                Log.i("vehicleReplacementDays", vehicleReplacmentDays);
                Log.i("vehicleValue", vehicleValue);
                Log.i("ncp", ncb);
                Log.i("coverCode", coverCode);
                Log.i("addCovers", addCovers);
                Log.i("cpr", cpr);

                params.put("policyStartDate", policyStartDate);
                params.put("noYearLicense", noYearLicense);
                params.put("regNo", regNo);
                params.put("chassisNo", chassisNo);
                params.put("vehicleType", vehicleType);
                params.put("bodyType", bodyType);
                params.put("make", make);
                params.put("modelType", modelType);
                params.put("model", model);
                params.put("ccHp", ccHp);
                params.put("cylindersNo", cylindersNo);
                params.put("seatingNo", seatingNo);
                params.put("regMonth", regMonth);
                params.put("vehicleReplacmentDays", vehicleReplacmentDays);
                params.put("vehicleValue", vehicleValue);
                params.put("ncb", ncb);
                params.put("coverCode", coverCode);
                params.put("addCover", addCovers);
                params.put("cpr", cpr);
                params.put("saveQuote", "0");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void getSendQuoteResult(JSONObject jsonObject) {
        try {
            Log.i("getSendQuoteResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                double premium = jsonObject.getDouble("result");
                double sumAssured = jsonObject.getDouble("totalSA");

                Intent intent = new Intent(this, SaveQuoteMotorActivity.class);
                intent.putExtra("policyStartDate", policyStartDate);
                intent.putExtra("noYearLicense", noYearLicense);
                intent.putExtra("regNo", regNo);
                intent.putExtra("chassisNo", chassisNo);
                intent.putExtra("vehicleType", vehicleType);
                intent.putExtra("bodyType", bodyType);
                intent.putExtra("make", make);
                intent.putExtra("modelType", modelType);
                intent.putExtra("model", model);
                intent.putExtra("ccHp", ccHp);
                intent.putExtra("cylindersNo", cylindersNo);
                intent.putExtra("seatingNo", seatingNo);
                intent.putExtra("seatingNo", seatingNo);
                intent.putExtra("regMonth", regMonth);
                intent.putExtra("vehicleReplacmentDays", vehicleReplacmentDays);
                intent.putExtra("vehicleValue", vehicleValue);
                intent.putExtra("ncb", ncb);
                intent.putExtra("coverCode", coverCode);
                intent.putExtra("addCover", addCovers);
                intent.putExtra("premium", premium);
                intent.putExtra("sumAssured", sumAssured);
                intent.putExtra("cpr", cpr);
                startActivity(intent);
            } else {
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE).
                        setAction(R.string.dialog_button_default, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                MyMethods.hideKeyboard(this);
                View view = View.inflate(this, R.layout.dialog_term_condition, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                final CheckBox checkBox = (CheckBox) view.findViewById(R.id.cbTermAccept);
                builder.setView(view)
                        .setCancelable(false)
                        .setPositiveButton(R.string.dialog_button_continue, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (checkBox.isChecked()) {
                                    prepareSendQuote();
                                } else {
                                    Toast.makeText(QuoteMotorActivity.this, R.string.message_mustAcceptTerm, Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Mint.transactionCancel("Motor Policy", "Back Pressed");
    }
}
