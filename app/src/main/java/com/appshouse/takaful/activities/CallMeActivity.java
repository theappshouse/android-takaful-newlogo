package com.appshouse.takaful.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/10/2016.
 */


public class CallMeActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView tvProductName;
    EditText etName, etDate, etTime, etPhone;
    LinearLayout lloDatePicker, lloTimePicker;
    SharedPreferencesClass setting;
    String productName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_call_me);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.pageTitle_callMe);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        setting = new SharedPreferencesClass(this);

        tvProductName = (TextView) findViewById(R.id.tvProductName);
        etName = (EditText) findViewById(R.id.etName);
        etDate = (EditText) findViewById(R.id.etDate);
        etTime = (EditText) findViewById(R.id.etTime);
        etPhone = (EditText) findViewById(R.id.etPhone);

        lloDatePicker = (LinearLayout) findViewById(R.id.lloDatePicker);
        lloTimePicker = (LinearLayout) findViewById(R.id.lloTimePicker);

        productName = getIntent().getStringExtra("productName");
        tvProductName.setText(productName);

        lloDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPickDateDialog();
            }
        });

        lloTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimePickerDialog();
            }
        });

        //auto set user name and phone if he is login otherwise the user will input the name and phone manually.
        if (setting.isLogin()) {
            getUserInfo();
        } else {
            //We enable the editTexts so the user fill his info manually.
            etName.setEnabled(true);
            etPhone.setEnabled(true);
        }
    }

    private void openPickDateDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mYear, mMonth, mDay;
                mYear = String.valueOf(year);
                mMonth = String.valueOf(monthOfYear + 1);
                mDay = String.valueOf(dayOfMonth);
                String date = mDay + "/" + mMonth + "/" + mYear;

                Calendar selectedDate = Calendar.getInstance();
                selectedDate.setTime(new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime());

                if ((selectedDate.getTimeInMillis() / 1000 + 86400) < (c.getTimeInMillis() / 1000)) {
                    Snackbar.make(findViewById(R.id.main), R.string.message_invalidDate, Snackbar.LENGTH_LONG).show();
                } else if (selectedDate.get(Calendar.DAY_OF_WEEK) == 6 || selectedDate.get(Calendar.DAY_OF_WEEK) == 7) {
                    Snackbar.make(findViewById(R.id.main), R.string.invalid_dateBetweenSunThu, Snackbar.LENGTH_LONG).show();
                } else {
                    etDate.setText(date);
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    private void openTimePickerDialog() {
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
                if (selectedHour < 16 && selectedHour >= 8)
                    etTime.setText(addZeroIfNeeded(selectedHour) + ":" + addZeroIfNeeded(selectedMinute));
                else
                    Snackbar.make(findViewById(R.id.main), R.string.Invalid_timeBetween8Am4Pm, Snackbar.LENGTH_LONG).show();
            }
        }, 8, 0, true);
        timePickerDialog.show();
    }

    private String addZeroIfNeeded(int number) {
        String sNumber = String.valueOf(number);
        if (sNumber.length() == 1)
            sNumber = "0" + sNumber;
        return sNumber;
    }

    String Tag_Request = "userInfo";
    private void getUserInfo() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetUserInfo
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                setUserInfo(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                MyMethods.showNetworkErrorDialog(CallMeActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getUserInfo();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("cpr", setting.getCpr());
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(CallMeActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setUserInfo(JSONObject jsonObject) {
        try {
            Log.i("getUserInfo _ Call", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                JSONObject jsonResult = jsonObject.getJSONObject("result");
                etName.setText(jsonResult.getString("firstName") + jsonResult.getString("middleName")
                        + jsonResult.getString("lastName"));
                etPhone.setText(jsonResult.getString("mobileNo"));
            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else {
                MyMethods.showNetworkErrorDialog(CallMeActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getUserInfo();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String date, name, time, phone;
    private void prepareSendEmail() {
        name = etName.getText().toString();
        phone = etPhone.getText().toString();
        date = etDate.getText().toString();
        time = etTime.getText().toString();

        if (name.isEmpty() || phone.trim().isEmpty()) {
            Snackbar.make(findViewById(R.id.main), R.string.message_enterRequiredData, Snackbar.LENGTH_LONG).show();
        } else {
            sendEmail();
        }
    }

    private void sendEmail() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.SendProductDetailEmail
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getSendEmailResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                sendEmail();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("productName", productName);
                params.put("name", name);
                params.put("phone", phone);
                params.put("date", date);
                params.put("time", time);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void getSendEmailResult(JSONObject jsonObject) {
        try {
            Log.i("getSendEmailResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                MyMethods.showGeneralDialogMessage(this,
                        R.string.message_requestSendSuccess, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
            } else {
                MyMethods.showSnackBarMessage(findViewById(R.id.main), jsonObject.getString(getString(R.string.api_message)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                MyMethods.hideKeyboard(this);
                prepareSendEmail();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
