package com.appshouse.takaful.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

/**
 * Created by Mohammed Algassab on 2/15/2016.
 */
public class MyNoteActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView tvNote;
    EditText etNote;
    SharedPreferencesClass setting;
    MenuItem confirm, edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_note);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_myNote);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        tvNote = (TextView) findViewById(R.id.tvNote);
        tvNote.setMovementMethod(new ScrollingMovementMethod());
        etNote = (EditText) findViewById(R.id.etNote);

        setting = new SharedPreferencesClass(this);
        tvNote.setText(setting.getNote());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_note, menu);
        confirm = menu.findItem(R.id.confirm);
        confirm.setVisible(false);
        edit = menu.findItem(R.id.edit);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                MyMethods.hideKeyboard(this);
                confirm.setVisible(false);
                edit.setVisible(true);

                etNote.setVisibility(View.GONE);
                tvNote.setVisibility(View.VISIBLE);
                tvNote.setText(etNote.getText());
                setting.saveNote(etNote.getText().toString());
                break;

            case R.id.edit:
                edit.setVisible(false);
                confirm.setVisible(true);

                etNote.setVisibility(View.VISIBLE);
                tvNote.setVisibility(View.GONE);
                etNote.setText(tvNote.getText());
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
