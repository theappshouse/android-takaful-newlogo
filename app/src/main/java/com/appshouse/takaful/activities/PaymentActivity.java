package com.appshouse.takaful.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.constant.PolicyStatus;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/8/2016.
 */
public class PaymentActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView tvPremium, tvNo;
    RadioButton rdCrediMax, rdDebitCard;
    double premium;
    SharedPreferencesClass setting;
    View view;
    String payFor , quoute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        view = findViewById(android.R.id.content);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_paymentOption);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        tvPremium = (TextView) findViewById(R.id.tvPremium);
        tvNo = (TextView) findViewById(R.id.tvNo);
        rdCrediMax = (RadioButton) findViewById(R.id.rdCrediMax);
        rdDebitCard = (RadioButton) findViewById(R.id.rdDebitCard);

        setting = new SharedPreferencesClass(this);

        premium = getIntent().getDoubleExtra("premium", 0);
        payFor = getIntent().getStringExtra("payFor");
        quoute = getIntent().getStringExtra("no") ;

        Log.d("quoteno " , quoute) ;

        //if it renewal the title will be policy no otherwise(new) it will be quotation no
        if (payFor.contentEquals("renewal")){
            ((TextView) findViewById(R.id.tvNoTitle)).setText(getString(R.string.view_policy_no));
            tvPremium.setText(String.valueOf(getString(R.string.bd) + premium + "   " + String.valueOf(getString(R.string.inclusive_of_vat)) ));

        }
        if(payFor.contentEquals("issue")){
            Log.d("quoteno", "issue");
            tvPremium.setText(String.valueOf(getString(R.string.bd) + premium + "   " + String.valueOf(getString(R.string.inclusive_of_vat)) ));

        }


        if (premium != 0) {
            MyMethods.showMainView(view);
            if (quoute.contains("GATT")){
                Log.d("quoteno", "inloopoffunctionquotTTT");
                tvPremium.setText(String.valueOf(getString(R.string.bd) + premium ));
               // tvPremium.setText(String.valueOf(getString(R.string.bd) + premium + "   " + String.valueOf(getString(R.string.inclusive_of_vat)) ));

            }else {
                tvPremium.setText(String.valueOf(getString(R.string.bd) + premium + "   " + String.valueOf(getString(R.string.inclusive_of_vat)) ));
            }


        } else {
            /** from myTakaful page when coming to this page form MyTakafulAdapter
             * @see com.appshouse.takaful.adapters.MyTakafulAdapter
             */
            getPremium();
        }
        tvNo.setText(getIntent().getStringExtra("no"));
    }

    String Tag_Request = "get_premium";

    private void getPremium() {
        Log.i("getPremium", "getPremium");
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetPolicyPremium
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setPremiumResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(PaymentActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPremium();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("no", getIntent().getStringExtra("no"));
                params.put("payFor", payFor);
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("cpr", setting.getCpr());
                params.put("deviceId", MyMethods.getDeviceId(PaymentActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setPremiumResult(JSONObject jsonObject) {
        try {
            Log.i("setPremiumResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                if (payFor.contentEquals("new"))
                    premium = jsonObject.getDouble("result");
                else
                    premium = Double.parseDouble(jsonObject.getJSONObject("result").getString("policyNetPremium"));

                if (quoute.contains("GATT")){
                    Log.d("quoteno", "inloopoffunctionquotTTT");
                    tvPremium.setText(String.valueOf(getString(R.string.bd) + premium ));
                    // tvPremium.setText(String.valueOf(getString(R.string.bd) + premium + "   " + String.valueOf(getString(R.string.inclusive_of_vat)) ));

                }else {
                    tvPremium.setText(String.valueOf(getString(R.string.bd) + premium + "   " + String.valueOf(getString(R.string.inclusive_of_vat)) ));
                }
                //tvPremium.setText(String.valueOf(premium) + "  " +String.valueOf(getString(R.string.inclusive_of_vat)) );
            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else {
                MyMethods.showNetworkErrorDialog(PaymentActivity.this, getString(R.string.failed_retrieve_premium),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getPremium();
                            }
                        });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String Tag_Request_Status = "getPolicyStatus";

    private void getPolicyStatus() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();

        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetPolicyStatus
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                setCheckPolicyStatus(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(PaymentActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getPolicyStatus();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("no", getIntent().getStringExtra("no"));
                params.put("payFor", payFor);
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("cpr", setting.getCpr());
                params.put("deviceId", MyMethods.getDeviceId(PaymentActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request_Status);
    }


    private void setCheckPolicyStatus(JSONObject response) {
        try {
            Log.i("setCheckPolicyStatus", response.toString());
            if (response.getBoolean("success")) {
                int policyStatus = response.getInt("result");

                if (policyStatus == PolicyStatus.APPROVED || policyStatus == PolicyStatus.RENEWAL_IN_PROGRESS) {
                    preparePaymentQuote();
                } else {
                    Snackbar.make(findViewById(R.id.main), R.string.message_policy_cant_paid, Snackbar.LENGTH_INDEFINITE)
                            .setAction(getString(R.string.retry), new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    getPolicyStatus();
                                }
                            }).show();
                }
            } else if (response.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else {
                MyMethods.showNetworkErrorDialog(PaymentActivity.this, getString(R.string.failed_retrieve_premium),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getPolicyStatus();
                            }
                        });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //String link = "http://46.101.130.180/paymentMethod.php?payment=DebitCard&payFor=new&payFor_id=Q/1299MOTR000835&platform=Android&cpr=510000541&totalPrice=0.001";
    String link;


    private void preparePaymentQuote() {
        String no, cpr, paymentType, platform;
        no = getIntent().getStringExtra("no");
        cpr = new SharedPreferencesClass(this).getCpr();
        paymentType = rdCrediMax.isChecked() ? "CrediMax" : "DebitCard";
        platform = "Android";


        link = APIs.PaymentURL + "?payment=" + paymentType + "&payFor=" + payFor +
                "&payFor_id=" + no + "&platform=" + platform + "&cpr=" + cpr;


        Log.i("link", link);

        Intent intent = new Intent(PaymentActivity.this, WebViewPaymentActivity.class);
        intent.putExtra("link", link);

        //We are sending the no and payFor again in case there is error in payment and the app need to issue or renew the no again.
        intent.putExtra("no", no);
        intent.putExtra("payFor", payFor);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                getPolicyStatus();
                //preparePaymentQuote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
