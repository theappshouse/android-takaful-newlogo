package com.appshouse.takaful.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.SpinnerAdapter;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.InputFilterMinMax;
import com.appshouse.takaful.utilities.MyMethods;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/1/2016.
 */
public class QuoteTravelActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Toolbar toolbar;
    LinearLayout lloDepartmentPickDate, lloPolicyStartPickDate;
    Typeface font;
    EditText etTravelNo, etPolicyStartDate, etDateOfDep, etTravelDestination;

    Spinner sAreaCoverage, sPlanType, sDuration;
    String[] selectedPlanTypeList, selectedPlanTypeValueList;
    String selectedPlanTypeValue, selectedAreaCoverageValue, selectedDurationValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quote_travel);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_getTravelQuote);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        etTravelNo = (EditText) findViewById(R.id.etTravelNo);
        etPolicyStartDate = (EditText) findViewById(R.id.etPolicyStartDate);
        etDateOfDep = (EditText) findViewById(R.id.etDateOfDep);
        etTravelDestination = (EditText) findViewById(R.id.etTravelDestination);

        sAreaCoverage = (Spinner) findViewById(R.id.sAreaCoverage);
        sPlanType = (Spinner) findViewById(R.id.sPlanType);
        sDuration = (Spinner) findViewById(R.id.sDuration);


        lloPolicyStartPickDate = (LinearLayout) findViewById(R.id.lloPolicyStartPickDate);
        lloDepartmentPickDate = (LinearLayout) findViewById(R.id.lloDepartmentPickDate);

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        font = MyMethods.getRegularFont(this);
        MyMethods.setFont(root, font);

        sAreaCoverage.setOnItemSelectedListener(this);
        sPlanType.setOnItemSelectedListener(this);
        sDuration.setOnItemSelectedListener(this);

//        lloPolicyStartPickDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openPickDateDialog(etPolicyStartDate);
//            }
//        });

        lloDepartmentPickDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPickDateDialog(etDateOfDep);
            }
        });
        setSpinnerData();
        //setAutoDemoData();
    }


    private void setAutoDemoData() {
        etPolicyStartDate.setText("01/03/2016");
        etTravelDestination.setText("India");
        etDateOfDep.setText("01/03/2016");
        sAreaCoverage.setSelection(1);
        sPlanType.setSelection(2);
        etTravelNo.setText("3");
    }

    private void openPickDateDialog(final EditText editText) {
        editText.requestFocus();

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mYear, mMonth, mDay;
                mYear = String.valueOf(year);
                mMonth = String.valueOf(monthOfYear + 1);
                mDay = String.valueOf(dayOfMonth);
                String date = mDay + "/" + mMonth + "/" + mYear;
//                try {
//                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//                    Calendar c = Calendar.getInstance();
//                    String currentDate =  sdf.format(c.getTime());
//                    Date strDate = sdf.parse(date);
//                    if (new Date().equals(strDate)) {
//                        editText.setText(date);
//                    }else {
//                        Snackbar.make(findViewById(R.id.main), R.string.message_invalidDate, Snackbar.LENGTH_SHORT).show();
//                    }
//                } catch (ParseException e) {
//                    e.printStackTrace();
//                }

                long dateTime = MyMethods.dateToSecond(date, "dd/MM/yyyy");
                if ((System.currentTimeMillis() / 1000) < (dateTime + 86400)) {
                    //editText.setText(date);
                    etPolicyStartDate.setText(date);
                    etDateOfDep.setText(date);
                } else {
                    Snackbar.make(findViewById(R.id.main), R.string.message_invalidDate, Snackbar.LENGTH_LONG).show();
                }
            }
        }, year, month, day);
        datePickerDialog.show();
    }


    private void setSpinnerData() {
        sAreaCoverage.setAdapter(new SpinnerAdapter(this, getResources().getStringArray(R.array.area_cover)));
        sDuration.setAdapter(new SpinnerAdapter(this, getResources().getStringArray(R.array.travel_duration)));
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sPlanType:
                selectedPlanTypeValue = selectedPlanTypeValueList[position];

                if (selectedPlanTypeValue.contentEquals("F")) {
                    etTravelNo.setEnabled(true);
                    etTravelNo.setFocusableInTouchMode(true);
                    etTravelNo.setFocusable(true);
                    etTravelNo.setFilters(new InputFilter[]{new InputFilterMinMax(2, 99)});
                    etTravelNo.setText("2");
                } else {
                    etTravelNo.setEnabled(false);
                    etTravelNo.setFocusable(false);
                    etTravelNo.setFilters(new InputFilter[]{new InputFilterMinMax(1, 1)});
                    etTravelNo.setText("1");
                }
                break;

            case R.id.sAreaCoverage:
                selectedAreaCoverageValue = getResources().getStringArray(R.array.area_cover_values)[position];
                setPlanType(position);
                break;

            case R.id.sDuration:
                selectedDurationValue = getResources().getStringArray(R.array.travel_duration_value)[position];
                break;
        }
    }

    private void setPlanType(int position) {
        String[] planType = getResources().getStringArray(R.array.plan_type);
        String[] planTypeValues = getResources().getStringArray(R.array.plan_type_values);
        switch (position) {
            case 0:
                selectedPlanTypeList = new String[1];
                selectedPlanTypeValueList = new String[1];
                selectedPlanTypeList[0] = planType[1];
                selectedPlanTypeValueList[0] = planTypeValues[1];
                break;
            case 1:
                selectedPlanTypeList = new String[3];
                selectedPlanTypeValueList = new String[3];
                selectedPlanTypeList[0] = planType[0];
                selectedPlanTypeList[1] = planType[2];
                selectedPlanTypeList[2] = planType[3];
                selectedPlanTypeValueList[0] = planTypeValues[0];
                selectedPlanTypeValueList[1] = planTypeValues[2];
                selectedPlanTypeValueList[2] = planTypeValues[3];
                break;
            case 2:
                selectedPlanTypeList = new String[2];
                selectedPlanTypeValueList = new String[2];
                selectedPlanTypeList[0] = planType[0];
                selectedPlanTypeList[1] = planType[3];
                selectedPlanTypeValueList[0] = planTypeValues[0];
                selectedPlanTypeValueList[1] = planTypeValues[3];
                break;
        }
        sPlanType.setAdapter(new SpinnerAdapter(this, selectedPlanTypeList));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    String policyStartDate, areaCoverage, planType, duration, departmentDate, destination, travelNo;

    private void prepareSendQuote() {
        policyStartDate = etPolicyStartDate.getText().toString();
        areaCoverage = selectedAreaCoverageValue;
        planType = selectedPlanTypeValue;
        duration = selectedDurationValue;
        departmentDate = etDateOfDep.getText().toString();
        destination = etTravelDestination.getText().toString();
        travelNo = etTravelNo.getText().toString();

        if (policyStartDate.isEmpty() || areaCoverage.isEmpty() || planType.isEmpty() ||
                duration.isEmpty() || departmentDate.isEmpty() || destination.isEmpty() || travelNo.isEmpty()) {
            Snackbar.make(findViewById(R.id.main), R.string.message_enterRequiredData, Snackbar.LENGTH_LONG).show();
        } else {
            if (Integer.parseInt(travelNo) > 0) {
                Log.i("policyStartDate", policyStartDate);
                Log.i("areaCoverage", areaCoverage);
                Log.i("planType", planType);
                Log.i("duration", duration);
                Log.i("departmentDate", departmentDate);
                Log.i("destination", destination);
                Log.i("travelNo", travelNo);

                sendQuote();
            } else {
                Snackbar.make(findViewById(R.id.main), R.string.traveler_least_1, Snackbar.LENGTH_LONG).show();
            }
        }
    }

    String Tag_Request = "send quote";

    private void sendQuote() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetSaveTravelQuote
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getSendQuoteResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                sendQuote();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("policyStartDate", policyStartDate);
                params.put("area", areaCoverage);
                params.put("planType", planType);
                params.put("period", duration);
                params.put("depDate", departmentDate);
                params.put("destination", destination);
                params.put("noTrav", travelNo);
                params.put("saveQuote", "0");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void getSendQuoteResult(JSONObject jsonObject) {
        try {
            Log.i("getSendQuoteResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                double premium = jsonObject.getDouble("result");
                String sumAssured = jsonObject.getString("totalSA");

                Intent intent = new Intent(this, SaveQuoteTravelActivity.class);
                intent.putExtra("policyStartDate", policyStartDate);
                intent.putExtra("area", areaCoverage);
                intent.putExtra("planType", planType);
                intent.putExtra("period", duration);
                intent.putExtra("depDate", departmentDate);
                intent.putExtra("destination", destination);
                intent.putExtra("noTrav", travelNo);
                intent.putExtra("cpr", getIntent().getStringExtra("cpr"));
                intent.putExtra("premium", premium);
                intent.putExtra("sumAssured", sumAssured);
                startActivity(intent);
            } else {
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE).
                        setAction(R.string.dialog_button_default, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                MyMethods.hideKeyboard(this);

                View view = View.inflate(this, R.layout.dialog_term_condition, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                final CheckBox checkBox = (CheckBox) view.findViewById(R.id.cbTermAccept);
                builder.setView(view)
                        .setCancelable(false)
                        .setPositiveButton(R.string.dialog_button_continue, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (checkBox.isChecked()) {
                                    prepareSendQuote();
                                } else {
                                    Toast.makeText(QuoteTravelActivity.this, R.string.message_mustAcceptTerm, Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
