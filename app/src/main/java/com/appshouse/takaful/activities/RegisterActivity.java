package com.appshouse.takaful.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/9/2016.
 */
public class RegisterActivity extends AppCompatActivity {
    Toolbar toolbar;
    EditText etCpr, etPassword, etPasswordConfirm, etEmail, etPhone, etPolicyNo, etMobileNo;
    Button bRegister;
    SharedPreferencesClass setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setting = new SharedPreferencesClass(this);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_register);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        etCpr = (EditText) findViewById(R.id.etCpr);
        etPassword = (EditText) findViewById(R.id.etPassword);
        etPasswordConfirm = (EditText) findViewById(R.id.etPasswordConfirm);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etPolicyNo = (EditText) findViewById(R.id.etPolicyNo);
        etMobileNo = (EditText) findViewById(R.id.etMobileNo);
        bRegister = (Button) findViewById(R.id.bRegister);

        etPassword.setGravity(setting.getLanguage().contentEquals("en") ? Gravity.LEFT : Gravity.RIGHT);
        etPasswordConfirm.setGravity(setting.getLanguage().contentEquals("en") ? Gravity.LEFT : Gravity.RIGHT);

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareRegister();
            }
        });
    }

    String cpr, password, passwordConfirm, email, policyNo, mobileNo;

    private void prepareRegister() {
        cpr = etCpr.getText().toString();
        password = etPassword.getText().toString();
        passwordConfirm = etPasswordConfirm.getText().toString();
        email = etEmail.getText().toString();
        policyNo = etPolicyNo.getText().toString();
        mobileNo = etMobileNo.getText().toString();

        if (cpr.isEmpty() || password.isEmpty() || passwordConfirm.isEmpty() || email.isEmpty()) {
            Snackbar.make(findViewById(R.id.main), R.string.message_enterRequiredData, Snackbar.LENGTH_LONG).show();
        } else if (!password.contentEquals(passwordConfirm)) {
            Snackbar.make(findViewById(R.id.main), R.string.message_passwordNotMatched, Snackbar.LENGTH_LONG).show();
        } else {
            Log.i("cpr", cpr);
            Log.i("password", password);
            Log.i("email", email);
            Log.i("policyNo", policyNo);
            Log.i("mobileNo", mobileNo);
            register();
        }
    }

    String Tag_Request = "register";
    ProgressDialog mDialog;

    private void register() {
        mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.Register
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if ((mDialog != null) && mDialog.isShowing())
                    mDialog.dismiss();
                getRegistrationResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if ((mDialog != null) && mDialog.isShowing())
                    mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                register();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("cpr", cpr);
                params.put("password", password);
                params.put("email", email);
                params.put("policyNo", policyNo);
                params.put("mobileNo", mobileNo);
                params.put("deviceId", MyMethods.getDeviceId(RegisterActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    //to handle exceptions.
    @Override
    public void onPause() {
        super.onPause();
        if ((mDialog != null) && mDialog.isShowing())
            mDialog.dismiss();
        mDialog = null;
    }

    private void getRegistrationResult(final JSONObject jsonObject) {
        try {
            Log.i("getSendQuoteResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                setting.setLogin(jsonObject.getInt("id"), cpr, email, jsonObject.getString("token"));
                MyMethods.showGeneralDialogMessage(this,
                        R.string.message_register_success, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        });
            } else {
                MyMethods.showSnackBarMessage(findViewById(R.id.main), jsonObject.getString(getString(R.string.api_message)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}


