package com.appshouse.takaful.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.constant.ProductCodes;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/8/2016.
 */
public class RenewDetailActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView tvPremium, tvSumInsured, tvStartDate, tvExpiryDate;
    SharedPreferencesClass setting;
    View view;
    String renewNo, productCode;
    String premium;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renew_detail);
        view = findViewById(android.R.id.content);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.renew_policy);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        tvPremium = (TextView) findViewById(R.id.tvPremium);
        tvSumInsured = (TextView) findViewById(R.id.tvSumInsured);
        tvStartDate = (TextView) findViewById(R.id.tvStartDate);
        tvExpiryDate = (TextView) findViewById(R.id.tvExpiryDate);

        setting = new SharedPreferencesClass(this);
        renewNo = getIntent().getStringExtra("no");
        productCode = getIntent().getStringExtra("productCode");
        getRenewDetail();
    }

    String Tag_Request = "get_premium";

    private void getRenewDetail() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetPolicyPremium
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setRenewDetail(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(RenewDetailActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getRenewDetail();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("no", renewNo);
                params.put("payFor", "renewal");
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("cpr", setting.getCpr());
                params.put("deviceId", MyMethods.getDeviceId(RenewDetailActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setRenewDetail(JSONObject jsonObject) {
        try {
            Log.i("setRenewDetail", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                JSONObject resultObject = jsonObject.getJSONObject("result");
                premium = resultObject.getString("policyNetPremium");
                tvPremium.setText(getString(R.string.bd)+ premium);
                tvStartDate.setText(resultObject.getString("policyStartDate"));
                tvExpiryDate.setText(resultObject.getString("policyExpiryDate"));
                tvSumInsured.setText(resultObject.getString("policySumInsured"));

            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else {
                MyMethods.showNetworkErrorDialog(RenewDetailActivity.this,  getString(R.string.failed_retrieve_premium),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getRenewDetail();
                            }
                        });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        changePolicyToActiveStatus(true);
        //super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        changePolicyToActiveStatus(false);
        super.onDestroy();
    }

    private void changePolicyToActiveStatus(final boolean showProgress) {
        if (getIntent().getStringExtra("productCode") != null)
            if (productCode.contentEquals(ProductCodes.MOTOR)) {
                final ProgressDialog mDialog = new ProgressDialog(this);
                if (showProgress) {
                    mDialog.setMessage(getString(R.string.progress_loading));
                    mDialog.show();
                }
                /**
                 That mean it is from myTakaful page motor renew
                 This is called only for renewal of motor because the status changed to 12 when getting
                 so if the user did not complete the process we change the status to previous by calling this api
                 */
                //TODO: should test the below api
                CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.RejectRenewPolicy
                        , null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        if (showProgress && !isFinishing())
                            mDialog.dismiss();

                        Log.i("onResponse", response.toString());
                        finish();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (showProgress)
                            mDialog.dismiss();
                        Log.i("VolleyError", error.getMessage());
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("policyNumber", getIntent().getStringExtra("no"));
                        params.put("userId", String.valueOf(setting.getUserId()));
                        params.put("token", setting.getAccessToken());
                        params.put("cpr", setting.getCpr());
                        params.put("deviceId", MyMethods.getDeviceId(RenewDetailActivity.this));
                        return params;
                    }
                };
                AppController.getInstance().addToRequestQueue(req);
            }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.next, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.next:
                View view = View.inflate(this, R.layout.dialog_term_condition, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                final CheckBox checkBox = (CheckBox) view.findViewById(R.id.cbTermAccept);
                builder.setView(view)
                        .setCancelable(false)
                        .setPositiveButton(R.string.dialog_button_continue, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (checkBox.isChecked()) {
                                    Intent intent = new Intent(RenewDetailActivity.this, PaymentActivity.class);
                                    intent.putExtra("no", renewNo);
                                    intent.putExtra("payFor", "renewal");
                                    if (!premium.isEmpty())
                                        intent.putExtra("premium", Double.parseDouble(premium));
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(RenewDetailActivity.this, R.string.message_mustAcceptTerm, Toast.LENGTH_LONG).show();
                                }
                            }
                        })
                        .setNegativeButton(R.string.dialog_button_cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        }).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
