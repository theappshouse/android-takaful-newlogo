package com.appshouse.takaful.attributes;

/**
 * Created by Mohammed Algassab on 5/12/2016.
 */
public class Directory {

    public int mImageRes;
    public String mTitle;
    public String mLink;

    public Directory(String title, int imageRes, String link) {
        mTitle = title;
        mLink = link;
        mImageRes = imageRes;
    }
}
