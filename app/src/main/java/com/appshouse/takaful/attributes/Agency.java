package com.appshouse.takaful.attributes;

/**
 * Created by Mohammed Algassab on 3/13/2016.
 */
public class Agency {

    public int mLogo;
    public String mLogoPath;
    public String mTitle;
    public String mName;
    public String mAddress;
    public String mPhone;
    public String mDaySales;
    public String mTimeSales;
    public String mDayService;
    public String mTimeService;
    public double mLatitude;
    public double mLongitude;

    public Agency(int logo, String title, String name, String address, String phone, String timeSales,
                  String timeService, double latitude, double longitude) {
        mLogo = logo;
        mTitle = title;
        mName = name;
        mAddress = address;
        mPhone = phone;
        mTimeSales = timeSales;
        mTimeService = timeService;
        mLatitude = latitude;
        mLongitude = longitude;
    }

    public Agency(String logo, String title, String name, String address, String phone, String daySales, String timeSales,
                  String dayService, String timeService, double latitude, double longitude) {
        mLogoPath = logo;
        mTitle = title;
        mName = name;
        mAddress = address;
        mPhone = phone;
        mDaySales = daySales;
        mTimeSales = timeSales;
        mDayService = dayService;
        mTimeService = timeService;
        mLatitude = latitude;
        mLongitude = longitude;
    }
}
