package com.appshouse.takaful.attributes;

/**
 * Created by Mohammed Algassab on 12/28/2015.
 */
public class SideMenu {
    public int mImageId;
    public String mTitle;

    public SideMenu(int imageId, String title) {
        mImageId = imageId;
        mTitle = title;
    }
}
