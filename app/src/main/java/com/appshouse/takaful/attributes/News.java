package com.appshouse.takaful.attributes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohammed Algassab on 5/9/2016.
 */
public class News implements Parcelable {

    public int mId;
    public String mTitle;
    public String mDesc;
    public String mDate;
    public String mImage;

    public News(int id, String title, String desc, String date, String image) {
        mId = id;
        mTitle = title;
        mDesc = desc;
        mDate = date;
        mImage = image;
    }

    public News(Parcel in) {
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in) {
        mId = in.readInt();
        mTitle = in.readString();
        mDesc = in.readString();
        mDate = in.readString();
        mImage = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mId);
        out.writeString(mTitle);
        out.writeString(mDesc);
        out.writeString(mDate);
        out.writeString(mImage);
    }

    public static final Parcelable.Creator<News> CREATOR = new Parcelable.Creator<News>() {
        public News createFromParcel(Parcel in) {
            return new News(in);
        }

        public News[] newArray(int size) {
            return new News[size];
        }

    };
}
