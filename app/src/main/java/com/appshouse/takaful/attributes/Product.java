package com.appshouse.takaful.attributes;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Mohammed Algassab on 1/4/2016.
 */
public class Product implements Parcelable {

    public int mId;
    public String mCode;
    public String mTitle;
    public String mShotDesc;
    public String mDesc;
    public String mImagePath;
    public String mThumbnail;
    public boolean mIsFeatured;
    public boolean mHasQuote;
    public String mPhone;

    public Product(int id, String code, String title, String shortDesc, String desc, String imagePath, String thumbnail,
                   boolean isFeatured, boolean hasQuote) {
        mId = id;
        mCode = code;
        mTitle = title;
        mShotDesc = shortDesc;
        mDesc = desc;
        mImagePath = imagePath;
        mThumbnail = thumbnail;
        mIsFeatured = isFeatured;
        mHasQuote = hasQuote;
        mPhone = "";

    }

    public Product(int id, String title, String desc) {
        mId = id;
        mTitle = title;
        mDesc = desc;
    }

    public Product(Parcel in) {
        readFromParcel(in);
    }

    private void readFromParcel(Parcel in) {
        mId = in.readInt();
        mCode = in.readString();
        mTitle = in.readString();
        mDesc = in.readString();
        mShotDesc = in.readString();
        mImagePath = in.readString();
        mThumbnail = in.readString();
        mIsFeatured = in.readByte() != 0;
        mHasQuote = in.readByte() != 0;
        mPhone = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(mId);
        out.writeString(mCode);
        out.writeString(mTitle);
        out.writeString(mDesc);
        out.writeString(mShotDesc);
        out.writeString(mImagePath);
        out.writeString(mThumbnail);
        out.writeByte((byte) (mIsFeatured ? 1 : 0));
        out.writeByte((byte) (mHasQuote ? 1 : 0));
        out.writeString(mPhone);
    }

    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }

    };
}
