package com.appshouse.takaful.attributes;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Mohammed Algassab on 2/13/2016.
 */
public class OurCenter {

    public String mName;
    public String mType;
    public LatLng mLocation;
    public String mTime;
    public int mTimeTextSize;
    public String mPhone;

    public OurCenter(String name,String type, LatLng location, String time, int timeTextSize, String phone) {
        mName = name;
        mType = type;
        mLocation = location;
        mTime = time;
        mTimeTextSize = timeTextSize;
        mPhone = phone;
    }
}
