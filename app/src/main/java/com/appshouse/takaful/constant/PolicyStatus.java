package com.appshouse.takaful.constant;

/**
 * Created by Mohammed Algassab on 3/20/2016.
 */
public class PolicyStatus {
    public static final int DRAFT = 1;
    public static final int REJECTED = 2;
    public static final int APPROVED = 3;
    public static final int RENEWED = 4;
    public static final int ACTIVE = 5;
    public static final int SUSPENDED = 8;
    public static final int CANCELLED = 9;
    public static final int MATURED = 13;
    public static final int RENEWAL_IN_PROGRESS = 12;
    public static final int FORFEITED = 17;
    public static final int LAPSED = 90;
}
