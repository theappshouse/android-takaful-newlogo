package com.appshouse.takaful.constant;

/**
 * Created by Mohammed Algassab on 3/17/2016.
 */
public class ProductCodes {
    public final static String MOTOR = "MOTR";
    public final static String TRAVEL = "GATT";
    public final static String FIRE = "FIRE";
    public final static String BAITAK = "FHHI";
}
