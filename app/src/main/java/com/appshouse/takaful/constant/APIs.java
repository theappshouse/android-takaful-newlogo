package com.appshouse.takaful.constant;

public class APIs {
    //testing
    //public static String URL = "https://mob.takafulweb.com/Testing/mobile_apis/";
    //public static String ImageURL = "https://mob.takafulweb.com/Testing/";
    //public static String PaymentURL = "http://46.101.130.180/Testing/paymentMethod.php";
    //public static String PaymentURL = "https://mob.takafulweb.com/Testing/paymentMethod.php" ;

    public static String URL = "https://mob.takafulweb.com/mobile_apis/";
    public static String ImageURL = "https://mob.takafulweb.com/";
    public static String PaymentURL = "https://mob.takafulweb.com/paymentMethod.php";
   // public static String PaymentURL = "https://mob.takafulweb.com/paymentMethodClosed.php";

    public static String PDF = URL + "pdf/";
    public static String GetStartDataForMotorQuote = URL + "get_start_data_for_motor_quote.php";
    public static String GetSaveMotorQuote = URL + "get_save_motor_quote.php";
    public static String GetProducts = URL + "get_products.php";
    public static String GetSaveTravelQuote = URL + "get_save_travel_quote.php";
    public static String Register = URL + "register.php";
    public static String GetUserInfo = URL + "get_user_info.php";
    public static String Logout = URL + "logout.php";
    public static String Login = URL + "login.php";
    public static String SendCodeToEmail = URL + "send_code_to_email.php";
    public static String ResetPassword = URL + "reset_password.php";
    public static String SendProductDetailEmail = URL + "send_product_request_email.php";
    public static String ApproveQuote = URL + "approve_quote.php";
    public static String GetMyTakaful = URL + "get_my_takaful.php";
    public static String GetPolicyPremium = URL + "get_policy_premuim.php";
    public static String GetClaims = URL + "get_claims.php";
    public static String GetMotorCardDetail = URL + "get_motor_card_detail.php";
    public static String GetMotorPolicies = URL + "get_motor_policies.php";
    public static String AddClaim = URL + "add_claim.php";
    public static String GetTravelCardDetail = URL + "get_travel_card_detail.php";
    public static String GetSaveFireQuote = URL + "get_save_fire_quote.php";
    public static String GetCities = URL + "get_cities.php";
    public static String SaveAddress = URL + "save_address.php";
    public static String RejectRenewPolicy = URL + "reject_renew_policy.php";
    public static String GetSaveBaitakQuote = URL + "get_save_baitak_quote.php";
    public static String GetFireBaitakCardDetail = URL + "get_fire_baitak_card_detail.php";
    public static String GetAgency = URL + "get_agency.php";
    public static String GetRoadAssistancePolicies = URL + "get_road_assist_policies.php";
    public static String SendRoadAssistanceEmail = URL + "send_road_assistance_email.php";
    public static String CheckUpdate = URL + "check_update.php";
    public static String IssueRenewQuote = URL + "issue_renew_policy.php";
    public static String GetSingleClaim = URL + "get_single_claim.php";
    //    public static String GetQuotationDetail = URL + "get_quotation_details.php";
    public static String GetQuotationDetail = URL + "get_quotation_details_v2.php";
    public static String GetNews = URL + "get_news.php";
    public static String GetNewsDetail = URL + "get_news_detail.php";
    public static String CreateNewClient = URL + "create_new_client.php";
    public static String GetPolicyStatus = URL + "get_policy_status.php";
    public static String UpdateMobile = URL + "update_mobile_user.php";

}
