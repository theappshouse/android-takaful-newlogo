package com.appshouse.takaful.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.utilities.MyMethods;


/**
 * Created by Mohammed Algassab on 3/2/2015.
 */
public class AddProgressDialog extends DialogFragment {
    ProgressBar pbAdDActivity;
    TextView tvProgressNumber;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_progress_add, container, false);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        pbAdDActivity = (ProgressBar) view.findViewById(R.id.pbAdDActivity);
        pbAdDActivity.setMax(100);
        pbAdDActivity.setProgress(0);
        tvProgressNumber = (TextView) view.findViewById(R.id.tvProgressNumber);

        ViewGroup root = (ViewGroup) view.findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(getActivity()));
        return view;
    }

    public void updateProgress(int progress) {
        if (pbAdDActivity != null) {
            pbAdDActivity.setProgress(progress);
            tvProgressNumber.setText(progress + "%");
        }
    }
}
