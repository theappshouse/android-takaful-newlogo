package com.appshouse.takaful.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.adapters.NewsAdapter;
import com.appshouse.takaful.attributes.News;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Mohammed Algassab on 1/14/2016.
 */
public class NewsFragment extends Fragment {

    RecyclerView lvNews;
    ArrayList<News> newsArrayList;
    RecyclerView.Adapter newsAdapter;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_news, container, false);

        lvNews = (RecyclerView) view.findViewById(R.id.lvNews);
        lvNews.setLayoutManager(new LinearLayoutManager(getActivity()));
        lvNews.setHasFixedSize(true);

        getNews();
        return view;
    }

    String Tag_Request = "get_News";
    private void getNews() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetNews
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setNews(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("VolleyError", error.toString());
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getNews();
                    }
                });
            }
        });
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setNews(JSONObject jsonObject) {
        try {
            Log.i("setProductQuote", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                newsArrayList = new ArrayList<>();
                JSONArray resultArray = jsonObject.getJSONArray("result");

                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject productObject = resultArray.getJSONObject(i);
                    News news = new News(productObject.getInt("Id"), productObject.getString("News_Title"),
                            productObject.getString("News_Details"), productObject.getString("News_Date"),
                            productObject.getString("News_Image"));
                    newsArrayList.add(news);
                }

                newsAdapter = new NewsAdapter(getActivity(), newsArrayList);
                lvNews.setAdapter(newsAdapter);
            } else {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getNews();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppController.getInstance().cancelPendingRequests(Tag_Request);
    }
}
