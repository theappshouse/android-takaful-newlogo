package com.appshouse.takaful.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.OurCenterAdapter;
import com.appshouse.takaful.attributes.OurCenter;
import com.appshouse.takaful.utilities.MyMethods;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mohammed Algassab on 2/13/2016.
 */
public class OurCenterFragment extends Fragment {
    private List<OurCenter> ourCenterList;
    RecyclerView lvOurCenter;
    RecyclerView.Adapter ourCenterAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_our_center, container, false);
        lvOurCenter = (RecyclerView) view.findViewById(R.id.lvOurCenter);
        lvOurCenter.setLayoutManager(new LinearLayoutManager(getActivity()));
        lvOurCenter.setHasFixedSize(true);
        ((TextView) view.findViewById(R.id.tvTitle)).setTypeface(MyMethods.getRegularFont(getActivity()));
        setDemoData();
        return view;
    }


    private void setDemoData() {
        int timeSize = getResources().getDimensionPixelSize(R.dimen.textSize_rowCenterTime);
        int timeSizeSmall = getResources().getDimensionPixelSize(R.dimen.textSize_rowCenterTimeSmall);
        ourCenterList = new ArrayList<>();
        /*Head Office*/
        ourCenterList.add(new OurCenter(getString(R.string.head_office), getString(R.string.insurance), new LatLng(26.238440, 50.538477), "Sun - Wed (8:00 - 4:00)\nThu (8:00 - 3:00)", timeSizeSmall, "17565656"));
        //ourCenterList.add(new OurCenter(getString(R.string.head_office), getString(R.string.insurance), new LatLng(26.238440, 50.538477), "Sun - Thu\n8:30 - 2:00", timeSizeSmall, "17565656"));
        /*Head Office*/

        /*Muharraq*/
        ourCenterList.add(new OurCenter(getString(R.string.muharraq_center), getString(R.string.insurance_and_claim), new LatLng(26.257248, 50.628446), "Sat - Thu\n8:00 - 4:00", timeSize, "17565400"));
       // ourCenterList.add(new OurCenter(getString(R.string.muharraq_center), getString(R.string.insurance), new LatLng(26.257248, 50.628446), "Sat - Thu\n9:00 - 2:30", timeSize, "17565400"));
        /*Muharraq*/

        /*Jerdab*/
        ourCenterList.add(new OurCenter(getString(R.string.jerdap_center), getString(R.string.insurance), new LatLng(26.166586, 50.569048), "Sat - Thu\n8:00 - 4:00", timeSize, "17565444"));
       // ourCenterList.add(new OurCenter(getString(R.string.jerdap_center), getString(R.string.insurance), new LatLng(26.166586, 50.569048), "Sat - Thu\n9:00 - 2:30", timeSize, "17565475"));
        /*Jerdab*/

        /*Salmabad*/
        ourCenterList.add(new OurCenter("Salmabad Center", "", new LatLng(26.177989, 50.531486), "Sun - Thu\n8:00 - 4:00", timeSize, "17565414"));
        //ourCenterList.add(new OurCenter(getString(R.string.hamala_center), getString(R.string.insurance), new LatLng(26.166714, 50.468018), "Sun - Thu\n9:00 - 2:30", timeSize, "17565447"));
        /*Hamala*/

        /*Hamala*/
        ourCenterList.add(new OurCenter(getString(R.string.hamala_center), getString(R.string.insurance), new LatLng(26.166714, 50.468018), "Sun - Thu\n8:00 - 4:00", timeSize, "17565447"));
        //ourCenterList.add(new OurCenter(getString(R.string.hamala_center), getString(R.string.insurance), new LatLng(26.166714, 50.468018), "Sun - Thu\n9:00 - 2:30", timeSize, "17565447"));
        /*Hamala*/

        /*seef_infita*/
        ourCenterList.add(new OurCenter(getString(R.string.Seef_center), getString(R.string.insurance), new LatLng(26.1720731, 50.5473259), "", timeSize, "13304050"));
        //ourCenterList.add(new OurCenter(getString(R.string.Seef_center), getString(R.string.insurance), new LatLng(26.1720731, 50.5473259), "", timeSize, "13304050"));
        /*seef_infita*/




        //ourCenterList.add(new OurCenter(getString(R.string.salmaba_center), getString(R.string.insurance_and_claim), new LatLng(26.177989, 50.531486), "Sun - Thu\n8:00 - 4:00", timeSize, "17565414"));





        ourCenterAdapter = new OurCenterAdapter(getActivity(), ourCenterList);
        lvOurCenter.setAdapter(ourCenterAdapter);
    }

}
