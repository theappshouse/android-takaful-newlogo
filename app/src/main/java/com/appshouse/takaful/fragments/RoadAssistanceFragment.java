package com.appshouse.takaful.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.adapters.SpinnerAdapter;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/12/2016.
 */
public class RoadAssistanceFragment extends Fragment implements AdapterView.OnItemSelectedListener,
        View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    Spinner sPolicyNumber;
    String[] policyNoArray, serviceProviderArray, quotationNoArray;
    String currentType, currentPolicyNo, currentQuotationNo, currentLatitude, currentLongitude;
    TextView tvCarName, tvRegNo;
    View view;
    LinearLayout lloAAA, lloGulf;
    LinearLayout lloBahrainCallAAA, lloInternationalCall;
    LinearLayout lloBahrainCallGulf, lloSaudiArabiaCall, lloKuwaitCallGulf, lloUAECall,
            lloQatarCallGulf, lloOmanCall, lloJordenCallGulf, lloSendLocation;
    GoogleApiClient mGoogleApiClient;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_road_assistance, container, false);
        ViewGroup root = (ViewGroup) view.findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(getActivity()));

        sPolicyNumber = (Spinner) view.findViewById(R.id.sPolicyNumber);
        tvCarName = (TextView) view.findViewById(R.id.tvCarName);
        tvRegNo = (TextView) view.findViewById(R.id.tvRegNo);
        lloAAA = (LinearLayout) view.findViewById(R.id.lloAAA);
        lloGulf = (LinearLayout) view.findViewById(R.id.lloGulf);

        lloBahrainCallAAA = (LinearLayout) view.findViewById(R.id.lloBahrainCallAAA);
        lloInternationalCall = (LinearLayout) view.findViewById(R.id.lloInternationalCall);
        lloBahrainCallGulf = (LinearLayout) view.findViewById(R.id.lloBahrainCallGulf);
        lloSaudiArabiaCall = (LinearLayout) view.findViewById(R.id.lloSaudiArabiaCall);
        lloKuwaitCallGulf = (LinearLayout) view.findViewById(R.id.lloKuwaitCallGulf);
        lloUAECall = (LinearLayout) view.findViewById(R.id.lloUAECall);
        lloQatarCallGulf = (LinearLayout) view.findViewById(R.id.lloQatarCallGulf);
        lloOmanCall = (LinearLayout) view.findViewById(R.id.lloOmanCall);
        lloJordenCallGulf = (LinearLayout) view.findViewById(R.id.lloJordenCallGulf);
        lloSendLocation = (LinearLayout) view.findViewById(R.id.lloSendLocation);

        lloBahrainCallAAA.setOnClickListener(this);
        lloInternationalCall.setOnClickListener(this);
        lloBahrainCallGulf.setOnClickListener(this);
        lloSaudiArabiaCall.setOnClickListener(this);
        lloKuwaitCallGulf.setOnClickListener(this);
        lloUAECall.setOnClickListener(this);
        lloQatarCallGulf.setOnClickListener(this);
        lloOmanCall.setOnClickListener(this);
        lloJordenCallGulf.setOnClickListener(this);
        lloSendLocation.setOnClickListener(this);

        sPolicyNumber.setOnItemSelectedListener(this);
        getRoadAssistancePolicies();


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lloBahrainCallAAA:
                MyMethods.dailCall(getActivity(), getString(R.string.phone_bahrain_aaa));
                break;
            case R.id.lloInternationalCall:
                MyMethods.dailCall(getActivity(), getString(R.string.phone_international));
                break;
            case R.id.lloBahrainCallGulf:
                MyMethods.dailCall(getActivity(), getString(R.string.phone_bahrain_gulf));
                break;
            case R.id.lloSaudiArabiaCall:
                MyMethods.dailCall(getActivity(), getString(R.string.phone_saudi));
                break;
            case R.id.lloKuwaitCallGulf:
                MyMethods.dailCall(getActivity(), getString(R.string.phone_kuwait));
                break;
            case R.id.lloUAECall:
                MyMethods.dailCall(getActivity(), getString(R.string.phone_uae));
                break;
            case R.id.lloQatarCallGulf:
                MyMethods.dailCall(getActivity(), getString(R.string.phone_qatar));
                break;
            case R.id.lloOmanCall:
                MyMethods.dailCall(getActivity(), getString(R.string.phone_oman));
                break;
            case R.id.lloJordenCallGulf:
                MyMethods.dailCall(getActivity(), getString(R.string.phone_jordan));
                break;
            case R.id.lloSendLocation:
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setTitle(getString(R.string.dialog_title_permission));
                    dialog.setMessage(getString(R.string.dialog_message_permLocation));

                    dialog.setPositiveButton(getString(R.string.dialog_button_understand), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                        }
                    });
                    dialog.setCancelable(false);
                    dialog.show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
                break;
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
            mGoogleApiClient.connect();
        }
    }

    Runnable runner;
    Handler handler;
    int i = 0;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            final Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            i = 0;
            final ProgressDialog mDialog = new ProgressDialog(getActivity());
            mDialog.setMessage(getString(R.string.getting_location));
            mDialog.setCancelable(false);
            mDialog.show();
            runner = new Runnable() {
                @Override
                public void run() {
                    if (mLastLocation != null) {
                        mDialog.dismiss();
                        currentLatitude = String.valueOf(mLastLocation.getLatitude());
                        currentLongitude = String.valueOf(mLastLocation.getLongitude());
                        sendRoadAssistanceEmail();
                    } else {
                        i++;
                        if (i < 7) {
                            handler.postDelayed(runner, 3000);
                        } else {
                            mDialog.dismiss();
                            Snackbar.make(view, R.string.message_noLocation, Snackbar.LENGTH_INDEFINITE).
                                    setAction(R.string.dialog_button_default, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                        }
                                    }).show();
                        }
                    }
                }
            };
            handler = new Handler();
            handler.postDelayed(runner, 100);
        } else if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            MyMethods.rationalDialog(getActivity(), R.string.dialog_message_permStorage, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, 2);
                }
            });
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("onConnectionFailed", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.sPolicyNumber:
                currentType = serviceProviderArray[position];
                currentPolicyNo = policyNoArray[position];
                currentQuotationNo = quotationNoArray[position];
                getMotorDetail();
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void getMotorDetail() {
        //clear the info
        lloGulf.setVisibility(View.GONE);
        lloAAA.setVisibility(View.GONE);
        tvRegNo.setText("");
        tvCarName.setText("");

        String Tag_Request = "GetMotorDetail";
        final ProgressDialog mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.setCancelable(false);
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetMotorCardDetail
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                setMotorDetail(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Log.i("getMotorDetail", "onErrorResponse " + error.getMessage());
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getMotorDetail();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting = new SharedPreferencesClass(getActivity());
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(getActivity()));
                params.put("cpr", setting.getCpr());
                params.put("policyNumber", currentPolicyNo);
                params.put("quotationNo", currentQuotationNo);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setMotorDetail(JSONObject jsonObject) {
        Log.i("setMotorDetail", jsonObject.toString());
        try {
            if (jsonObject.getBoolean("success")) {
                JSONObject motorObject = jsonObject.getJSONObject("result");
                tvCarName.setText(motorObject.getString("makeAndModel"));
                tvRegNo.setText(motorObject.getString("regNumberChasisNumber"));

                if (currentType.contentEquals("AAA")) {
                    lloAAA.setVisibility(View.VISIBLE);
                } else {
                    lloGulf.setVisibility(View.VISIBLE);
                }
            } else {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getMotorDetail();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    String Tag_getRoadAssistancePolicies = "getRoadAssistancePolicies";

    private void getRoadAssistancePolicies() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetRoadAssistancePolicies
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setPolicies(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showMainView(view);
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getRoadAssistancePolicies();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting = new SharedPreferencesClass(getActivity());
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(getActivity()));
                params.put("cpr", setting.getCpr());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_getRoadAssistancePolicies);
    }

    private void setPolicies(JSONObject jsonObject) {
        Log.i("setPolicies", jsonObject.toString());
        try {
            if (jsonObject.getBoolean("success")) {
                JSONArray resultArray = jsonObject.getJSONArray("result");
                if (resultArray.length() > 0) {
                    policyNoArray = new String[resultArray.length()];
                    quotationNoArray = new String[resultArray.length()];
                    serviceProviderArray = new String[resultArray.length()];
                    for (int i = 0; i < resultArray.length(); i++) {
                        JSONObject policyObject = resultArray.getJSONObject(i);
                        policyNoArray[i] = policyObject.getString("policyNumber");
                        serviceProviderArray[i] = policyObject.getString("serviceProvider");
                        quotationNoArray[i] = policyObject.getString("quoteNo");
                    }

                    sPolicyNumber.setAdapter(new SpinnerAdapter(getActivity(), policyNoArray));
                } else {
                    view.findViewById(R.id.tvNoMotorPolicy).setVisibility(View.VISIBLE);
                }
            } else {

                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getRoadAssistancePolicies();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendRoadAssistanceEmail() {
        String Tag_Request = "SendRoadAssistanceEmail";
        final ProgressDialog mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.setCancelable(false);
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.SendRoadAssistanceEmail
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                sendEmailResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendRoadAssistanceEmail();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                SharedPreferencesClass setting = new SharedPreferencesClass(getActivity());
                params.put("latitude", currentLatitude);
                params.put("longitude", currentLongitude);
                params.put("policyNo", currentPolicyNo);
                params.put("carName", tvCarName.getText().toString());
                params.put("regNo", tvRegNo.getText().toString());
                params.put("cpr", setting.getCpr());
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void sendEmailResult(JSONObject response) {
        try {
            MyMethods.showSnackBarMessage(view, response.getString(getString(R.string.api_message)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppController.getInstance().cancelPendingRequests(Tag_getRoadAssistancePolicies);
    }

}
