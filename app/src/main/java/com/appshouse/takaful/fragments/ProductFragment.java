package com.appshouse.takaful.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.adapters.ProductAdapter;
import com.appshouse.takaful.attributes.Product;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Mohammed Algassab on 1/14/2016.
 */
public class ProductFragment extends Fragment {

    RecyclerView lvProduct;
    ArrayList<Product> productArrayList;
    RecyclerView.Adapter productAdapter;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_product, container, false);

        lvProduct = (RecyclerView) view.findViewById(R.id.lvProduct);
        lvProduct.setLayoutManager(new LinearLayoutManager(getActivity()));
        lvProduct.setHasFixedSize(true);

        getProducts();
        return view;
    }

    String Tag_Request = "get_products";
    private void getProducts() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.GET, APIs.GetProducts
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setProducts(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("VolleyError", error.toString());
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getProducts();
                    }
                });
            }
        });
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setProducts(JSONObject jsonObject) {
        try {
            Log.i("setProductQuote", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                productArrayList = new ArrayList<>();
                JSONArray resultArray = jsonObject.getJSONArray("result");

                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject productObject = resultArray.getJSONObject(i);
                    Product product = new Product(productObject.getInt("Id"), productObject.getString("Product_Code"),
                            productObject.getString(getString(R.string.z_product_name)), productObject.getString(getString(R.string.z_short_description)),
                            productObject.getString(getString(R.string.z_product_details)), productObject.getString("Product_Image"),
                            productObject.getString("Product_Thumbnail"), productObject.getInt("Is_Feature") == 1,
                            productObject.getInt("Has_Quote") == 1);

                    productArrayList.add(product);
                }

                productAdapter = new ProductAdapter(getActivity(), productArrayList);

                lvProduct.setAdapter(productAdapter);
            } else {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getProducts();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppController.getInstance().cancelPendingRequests(Tag_Request);
    }
}
