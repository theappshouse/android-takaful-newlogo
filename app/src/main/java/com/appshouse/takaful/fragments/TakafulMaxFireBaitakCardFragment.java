package com.appshouse.takaful.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appshouse.takaful.R;

/**
 * Created by Mohammed Algassab on 1/9/2016.
 */
public class TakafulMaxFireBaitakCardFragment extends Fragment {

    TextView tvName, tvFirstNumber, tvFinalNumber, tvPolicyStart, tvPolicyEnd,
            tvCover, tvSumCover, tvHouseNo, tvRoadNo, tvBlockNo, tvCity;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_pager_max_fire_baitak_card, container, false);
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvFirstNumber = (TextView) view.findViewById(R.id.tvFirstNumber);
        tvPolicyStart = (TextView) view.findViewById(R.id.tvPolicyStart);
        tvPolicyEnd = (TextView) view.findViewById(R.id.tvPolicyEnd);
        tvCover = (TextView) view.findViewById(R.id.tvCover);
        tvSumCover = (TextView) view.findViewById(R.id.tvSumCover);
        tvHouseNo = (TextView) view.findViewById(R.id.tvHouseNo);
        tvRoadNo = (TextView) view.findViewById(R.id.tvRoadNo);
        tvBlockNo = (TextView) view.findViewById(R.id.tvBlockNo);
        tvCity = (TextView) view.findViewById(R.id.tvCity);


        Bundle bundle = getArguments();
        if (bundle.getBoolean("ar")) {
            ((TextView) view.findViewById(R.id.tvPolicyStartTitle)).setText("إبتداء التأمين");
            ((TextView) view.findViewById(R.id.tvPolicyEndTitle)).setText("إنتهاء التأمين");
            ((TextView) view.findViewById(R.id.tvCoverTitle)).setText("نوع الغطاء");
            ((TextView) view.findViewById(R.id.tvSumCoverTitle)).setText("مجوع التأمين");

            ((TextView) view.findViewById(R.id.tvHouseTitle)).setText("المنزل");
            ((TextView) view.findViewById(R.id.tvRoadTitle)).setText("تاريخ الطريق");
            ((TextView) view.findViewById(R.id.tvBlockTitle)).setText("المجمع");
            ((TextView) view.findViewById(R.id.tvCityTitle)).setText("المنطقة");
        }
        tvName.setText(bundle.getString("name"));
        tvFirstNumber.setText(bundle.getString("firstNumber"));
        tvPolicyStart.setText(bundle.getString("policyStart"));
        tvPolicyEnd.setText(bundle.getString("policyEnd"));
        tvCover.setText(bundle.getString("cover"));
        tvSumCover.setText(bundle.getString("sumCover"));
        tvHouseNo.setText(bundle.getString("houseNo"));
        tvRoadNo.setText(bundle.getString("roadNo"));
        tvBlockNo.setText(bundle.getString("blockNo"));
        tvCity.setText(bundle.getString("city"));

        return view;
    }
}
