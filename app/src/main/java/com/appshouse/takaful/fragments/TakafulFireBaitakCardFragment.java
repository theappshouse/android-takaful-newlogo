package com.appshouse.takaful.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appshouse.takaful.R;

/**
 * Created by Mohammed Algassab on 1/9/2016.
 */
public class TakafulFireBaitakCardFragment extends Fragment {

    TextView tvName, tvFirstNumber, tvFinalNumber, tvPolicyStartDate, tvPolicyEndDate,
            tvCover, tvSumCover;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_pager_fire_baitak_card, container, false);
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvFirstNumber = (TextView) view.findViewById(R.id.tvFirstNumber);
        tvFinalNumber = (TextView) view.findViewById(R.id.tvFinalNumber);
        tvPolicyStartDate = (TextView) view.findViewById(R.id.tvPolicyStartDate);
        tvPolicyEndDate = (TextView) view.findViewById(R.id.tvPolicyEndDate);
        tvCover = (TextView) view.findViewById(R.id.tvCover);
        tvSumCover = (TextView) view.findViewById(R.id.tvSumCover);



        Bundle bundle = getArguments();
        if(bundle.getBoolean("ar")){
            ((TextView) view.findViewById(R.id.tvPolicyStartTitle)).setText("إبتداء التأمين");
            ((TextView) view.findViewById(R.id.tvPolicyEndTitle)).setText("إنتهاء التأمين");
            ((TextView) view.findViewById(R.id.tvCoverTitle)).setText("نوع التأمين");
            ((TextView) view.findViewById(R.id.tvSumCoverTitle)).setText("مجموع التأمين");
        }
        tvName.setText(bundle.getString("name"));
        tvFirstNumber.setText(bundle.getString("firstNumber"));
        tvPolicyStartDate.setText(bundle.getString("policyStartDate"));
        tvPolicyEndDate.setText(bundle.getString("policyEndDate"));
        tvCover.setText(bundle.getString("cover"));
        tvSumCover.setText(bundle.getString("sumCover"));

        return view;
    }
}
