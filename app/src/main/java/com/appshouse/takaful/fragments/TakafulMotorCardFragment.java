package com.appshouse.takaful.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appshouse.takaful.R;

/**
 * Created by Mohammed Algassab on 1/9/2016.
 */
public class TakafulMotorCardFragment extends Fragment {

    TextView tvName, tvFirstNumber, tvFinalNumber, tvCommencementDate, tvCommencementTime,
            tvExpiryDate, tvExpiryTime, tvVehicleYear, tvVehicleType, tvCover;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_pager_motor_card, container, false);
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvFirstNumber = (TextView) view.findViewById(R.id.tvFirstNumber);
        tvFinalNumber = (TextView) view.findViewById(R.id.tvFinalNumber);
        tvCommencementDate = (TextView) view.findViewById(R.id.tvCommencementDate);
        tvCommencementTime = (TextView) view.findViewById(R.id.tvCommencementTime);
        tvExpiryDate = (TextView) view.findViewById(R.id.tvExpiryDate);
        tvExpiryTime = (TextView) view.findViewById(R.id.tvExpiryTime);
        tvVehicleYear = (TextView) view.findViewById(R.id.tvVehicleYear);
        tvVehicleType = (TextView) view.findViewById(R.id.tvVehicleType);
        tvCover = (TextView) view.findViewById(R.id.tvCover);


        Bundle bundle = getArguments();
        if(bundle.getBoolean("ar")){
            ((TextView) view.findViewById(R.id.tvCommencementTitle)).setText("إبتداء التأمين");
            ((TextView) view.findViewById(R.id.tvExpiryTitle)).setText("إنتهاء التأمين");
            ((TextView) view.findViewById(R.id.tvVehicleTitle)).setText("المركبة المغطاة");
            ((TextView) view.findViewById(R.id.tvCoverTitle)).setText("نوع الغطاء");
        }
        tvName.setText(bundle.getString("name"));
        tvFirstNumber.setText(bundle.getString("firstNumber"));
        tvFinalNumber.setText(bundle.getString("finalNumber"));
        tvCommencementDate.setText(bundle.getString("commencementDate"));
        tvCommencementTime.setText(bundle.getString("commencementTime"));
        tvExpiryDate.setText(bundle.getString("expiryDate"));
        tvExpiryTime.setText(bundle.getString("expiryTime"));
        tvVehicleYear.setText(bundle.getString("vehicleYear"));
        tvVehicleType.setText(bundle.getString("vehicleType"));
        tvCover.setText(bundle.getString("cover"));

        return view;
    }
}
