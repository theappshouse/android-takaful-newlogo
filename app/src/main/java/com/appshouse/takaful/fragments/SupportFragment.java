package com.appshouse.takaful.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.appshouse.takaful.R;
import com.appshouse.takaful.utilities.MyMethods;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Mohammed Algassab on 2/13/2016.
 */
public class SupportFragment extends Fragment implements OnMapReadyCallback {

    View view;
    LinearLayout lloBahrainCall,lloAppsHouse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_support, container, false);

        lloBahrainCall = (LinearLayout) view.findViewById(R.id.lloBahrainCall);
        lloAppsHouse  = (LinearLayout) view.findViewById(R.id.lloAppsHouse);
        lloBahrainCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMethods.dailCall(getActivity(), "80008050");
            }
        });
        lloAppsHouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMethods.openLink(getActivity(),"http://theappshouse.com/");
            }
        });

        ViewGroup root = (ViewGroup) view.findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(getActivity()));

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(26.2387349, 50.5384357), 15));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(new LatLng(26.2387349, 50.5384357));
        googleMap.addMarker(markerOptions);
    }
}
