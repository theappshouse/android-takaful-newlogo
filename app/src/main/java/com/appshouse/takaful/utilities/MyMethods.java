package com.appshouse.takaful.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.MainActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Mohammed Algassab on 1/19/2015.
 */
public class MyMethods {

    public static void showSnackBarMessage(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE).
                setAction(R.string.dialog_button_default, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });
        ((TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text)).setMaxLines(5);
        snackbar.show();
    }



    public static boolean saveImageToExternalStorage(Context context, Bitmap image, String name) {
        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Takaful/";

        try {
            File dir = new File(fullPath);
            if (!dir.exists())
                dir.mkdirs();

            OutputStream fOut;
            File file = new File(fullPath, name + ".png");
            file.createNewFile();
            fOut = new FileOutputStream(file);

            image.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
            fOut.close();

            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(file);
            mediaScanIntent.setData(contentUri);
            context.sendBroadcast(mediaScanIntent);

            return true;
        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
            return false;
        }
    }

    public static void reloadActivity(Activity activity) {
        Intent intent = activity.getIntent();
        activity.overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        activity.finish();
        activity.overridePendingTransition(0, 0);
        activity.startActivity(intent);
    }

    public static Typeface getRegularFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), context.getString(R.string.z_fontRegular));
    }


    public static Typeface getBoldFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), context.getString(R.string.z_fontBold));
    }


    public static Typeface getLightFont(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/KelsonSansLight.ttf");
    }

    public static boolean isUpcomingTime(long seconds) {
        return (System.currentTimeMillis() / 1000) < seconds;
    }

    public static String secondsToDate(long seconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(new Date(seconds * 1000L));
    }

    public static long dateToSecond(String dateTime, String dateFormat) {
        SimpleDateFormat f = new SimpleDateFormat(dateFormat);
        try {
            Date d = f.parse(dateTime);
            return d.getTime() / 1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void downloadUrl(Context context, Uri uri, String fileName) {
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        DownloadManager dm = (DownloadManager) context.getSystemService(context.DOWNLOAD_SERVICE);
        dm.enqueue(request);
    }

//    public static boolean isNetworkAvailable(Context c) {
//        ConnectivityManager connectivityManager = (ConnectivityManager) c
//                .getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
//            return true;
//        } else {
//            Toast.makeText(c, c.getString(R.string.message_noNetworkConnection), Toast.LENGTH_SHORT).show();
//            return false;
//        }
//    }

    public static void showNetworkErrorDialog(final Context context, final DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.network_error);
        dialog.setMessage(context.getString(R.string.networkError));
        dialog.setPositiveButton(context.getString(R.string.retry), onClickListener);
        dialog.setNegativeButton(context.getString(R.string.back), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).onBackPressed();
            }
        });
        dialog.setCancelable(false);

        if (!((Activity) context).isFinishing())
            dialog.show();
    }


    public static void showNetworkErrorDialog(final Context context, String message, final DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.network_error);
        dialog.setMessage(message);
        dialog.setPositiveButton(context.getString(R.string.retry), onClickListener);
        dialog.setNegativeButton(R.string.back, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).onBackPressed();
            }
        });
        dialog.setCancelable(false);
        if (!((Activity) context).isFinishing())
            dialog.show();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    public static void setFont(ViewGroup group, Typeface font) {
        int count = group.getChildCount();
        View v;
        for (int i = 0; i < count; i++) {
            v = group.getChildAt(i);
            if (v instanceof TextView || v instanceof EditText || v instanceof Button) {
                ((TextView) v).setTypeface(font);
            } else if (v instanceof ViewGroup)
                setFont((ViewGroup) v, font);
        }
    }

//    public static void sendEmail(Context context, String emailAddress) {
//        Intent email = new Intent(Intent.ACTION_SEND);
//        email.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});
//        email.putExtra(Intent.EXTRA_SUBJECT, "subject");
//        email.putExtra(Intent.EXTRA_TEXT, "message");
//        email.setType("message/rfc822");
//        context.startActivity(Intent.createChooser(email, context.getString(R.string.intent_chooseEmail)));
//    }

    public static void dailCall(Context context, String phone) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + phone));
        context.startActivity(callIntent);
    }

    public static void openLink(Context context, String url) {
        Uri uri;
        if (!url.contains("http://") && !url.contains("https://"))
            uri = Uri.parse("http://" + url);
        else
            uri = Uri.parse(url);

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
        context.startActivity(browserIntent);
    }

    public static void showLoginPage(final Activity activity) {
        SharedPreferencesClass setting = new SharedPreferencesClass(activity);
        setting.logout();

        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle(activity.getString(R.string.dialog_title_unauthorizedAccess));
        dialog.setMessage(activity.getString(R.string.dialog_message_unauthorizedAccess));
        dialog.setPositiveButton(activity.getString(R.string.dialog_button_login), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(activity, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("isLoginPage", true);
                activity.startActivity(intent);
            }
        });
        dialog.setNegativeButton(activity.getString(R.string.dialog_button_cancel), null);
        dialog.show();
    }

    public static void showLoginPageAfterSaveQuote(final Activity activity, String policyNo) {
        SharedPreferencesClass setting = new SharedPreferencesClass(activity);
        setting.logout();

        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setTitle("Login Required");
        dialog.setMessage("In order to view your details, you are kindly required to login or register using your Policy No:: " + policyNo);
        dialog.setPositiveButton(activity.getString(R.string.dialog_button_login), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(activity, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("isLoginPage", true);
                activity.startActivity(intent);
            }
        });
        dialog.setNegativeButton(activity.getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(activity, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(intent);
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showGeneralDialogMessage(Activity activity, int message, DialogInterface.OnClickListener clickListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setMessage(message);
        dialog.setPositiveButton(activity.getString(R.string.dialog_button_default), clickListener);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void showGeneralDialogMessage(Activity activity, String message, DialogInterface.OnClickListener clickListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
        dialog.setMessage(message);
        dialog.setPositiveButton(activity.getString(R.string.dialog_button_default), clickListener);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static boolean isLogin(final Activity context, View view) {
        if (new SharedPreferencesClass(context).isLogin())
            return true;
        else {
            Snackbar.make(view, R.string.message_needLogin, Snackbar.LENGTH_LONG)
                    .setAction(context.getString(R.string.login), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("isLoginPage", true);
                            context.startActivity(intent);
                        }
                    }).show();
            return false;
        }
    }

    public static void showMainView(View view) {
        view.findViewById(R.id.lyLoading).setVisibility(View.GONE);
//        view.findViewById(R.id.lyNetworkError).setVisibility(View.GONE);
//        view.findViewById(R.id.lyNothingFound).setVisibility(View.GONE);
    }

    public static void showLoading(View view) {
//        view.findViewById(R.id.lyNetworkError).setVisibility(View.GONE);
//        view.findViewById(R.id.lyNothingFound).setVisibility(View.GONE);
        view.findViewById(R.id.lyLoading).setVisibility(View.VISIBLE);
    }
//
//    public static void showNothingFound(View view) {
//        view.findViewById(R.id.lyLoading).setVisibility(View.GONE);
//        view.findViewById(R.id.lyNetworkError).setVisibility(View.GONE);
//        view.findViewById(R.id.lyNothingFound).setVisibility(View.VISIBLE);
//        Random rand = new Random();
//        int randomNum = rand.nextInt((10 - 0) + 1) + 0;
//        if (randomNum >= 5) {
//            ((ImageView) view.findViewById(R.id.ivNothingFound)).setImageResource(R.drawable.nothing_found1);
//        } else {
//            ((ImageView) view.findViewById(R.id.ivNothingFound)).setImageResource(R.drawable.nothing_found2);
//        }
//    }
//
//    public static void showNetworkError(View view, final View.OnClickListener onClickListener) {
//        view.findViewById(R.id.lyLoading).setVisibility(View.GONE);
//        view.findViewById(R.id.lyNothingFound).setVisibility(View.GONE);
//        LinearLayout lyNetworkError = (LinearLayout) view.findViewById(R.id.lyNetworkError);
//        lyNetworkError.setVisibility(View.VISIBLE);
//        lyNetworkError.findViewById(R.id.bRetry).setOnClickListener(onClickListener);
//    }

    public static boolean isDateValid(long seconds) {
        if ((System.currentTimeMillis() / 1000) < seconds)
            return true;
        return false;
    }

    public static void changeLanguage(Activity activity, String lan) {
        Configuration configuration = new Configuration(activity.getResources().getConfiguration());
        configuration.locale = new Locale(lan);
        activity.getResources().updateConfiguration(configuration, activity.getResources().getDisplayMetrics());
    }


    public static void setLocale(Activity activity, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        activity.getApplicationContext().getResources().updateConfiguration(config, null);
    }

    public static Bitmap decodeFile(File f) {
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);
            final int REQUIRED_SIZE = 700;
            Log.i("School Test", "Scale = " + (o.outWidth + o.outHeight) / REQUIRED_SIZE);
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = (o.outWidth + o.outHeight) / REQUIRED_SIZE;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public static Bitmap decodeFile2(File f) throws IOException {
        Bitmap b = null;
        final int REQUIRED_SIZE = 1200;
        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        FileInputStream fis = null;
        fis = new FileInputStream(f);
        BitmapFactory.decodeStream(fis, null, o);
        fis.close();

        int scale = 1;
        if (o.outHeight > REQUIRED_SIZE || o.outWidth > REQUIRED_SIZE) {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(REQUIRED_SIZE /
                    (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }
        Log.i("School Test", "Scale = " + scale);
        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        fis = new FileInputStream(f);
        b = BitmapFactory.decodeStream(fis, null, o2);
        fis.close();

        return b;
    }

    public static int getImageFileOrientation(String photoPath) {
        try {
            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return 90;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return 180;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static Bitmap rotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null)
            ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void rationalDialog(Context context, int messageId, final DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle(R.string.dialog_title_permission);
        dialog.setMessage(messageId);
        dialog.setPositiveButton(context.getString(R.string.dialog_button_understand), onClickListener);
        dialog.setCancelable(false);
        dialog.show();
    }

    public static void rationalDialogAppSetting(final Activity activity, int message) {
        MyMethods.rationalDialog(activity, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                intent.setData(uri);
                activity.startActivityForResult(intent, 9);
            }
        });
    }

//    public static void showLoginPage(final Activity activity) {
//        SharedPreferencesClass setting = new SharedPreferencesClass(activity);
//        setting.logout();
//
//        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
//        dialog.setTitle(activity.getString(R.string.dialog_title_sessionExpired));
//        dialog.setMessage(activity.getString(R.string.dialog_message_sessionExpired));
//        dialog.setPositiveButton(activity.getString(R.string.dialog_button_login), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent intent = new Intent(activity, MainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                intent.putExtra("isLoginPage", true);
//                activity.startActivity(intent);
//            }
//        });
//        dialog.setNegativeButton(activity.getString(R.string.dialog_button_cancel), null);
//        dialog.show();
//    }

    public static void openGoogleMap(Context context, String latitude, String longitude, String lableName) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:" + latitude + ","
                + longitude + "?q=" + latitude + "," + longitude + "(" + lableName + ")"));
        context.startActivity(intent);
    }

}
