package com.appshouse.takaful.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesClass {
    public static final String PREFS_NAME = "takaful";
    public static final int PRIVATE_MODE = 0;
    public SharedPreferences setting;
    public SharedPreferences.Editor editor;
    Context context;

    public SharedPreferencesClass(Context context) {
        setting = context.getSharedPreferences(PREFS_NAME, PRIVATE_MODE);
        this.context = context;
    }

    public void setLogin(int userId, String cpr, String email, String accessToken) {
        editor = setting.edit();
        editor.putBoolean("login", true);
        editor.putInt("userId", userId);
        editor.putString("cpr", cpr);
        editor.putString("email", email);
        editor.putString("accessToken", accessToken);
        editor.commit();
    }

    public void logout() {
        editor = setting.edit();
        editor.putBoolean("login", false);
        editor.putInt("userId", 0);
        editor.putString("accessToken", "");
        editor.putString("cpr", "");
        editor.putString("email", "");
        editor.commit();
    }

    public void saveNote(String noteText) {
        editor = setting.edit();
        editor.putString("noteText", noteText);
        editor.commit();
    }

    public void changeLanguage(Activity activity ,String lan) {
        editor = setting.edit();
        editor.putString("language", lan);
        editor.commit();

        MyMethods.setLocale(activity, lan);
        MyMethods.reloadActivity((Activity) context);
    }

    public String getLanguage() {
        return setting.getString("language", "en");
    }

    public String getNote() {
        return setting.getString("noteText", "");
    }

    public int getUserId() {
        return setting.getInt("userId", 0);
    }

    public boolean isLogin() {
        return setting.getBoolean("login", false);
    }

    public String getCpr() {
        return setting.getString("cpr", "");
    }

    public String getEmail() {
        return setting.getString("email", "");
    }

    public String getPhone() {
        return setting.getString("phone", "");
    }

    public String getAccessToken() {
        return setting.getString("accessToken", "");
    }

    public void clearPerferance() {
        editor = setting.edit();
        editor.clear();
        editor.commit();
    }
}
